import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Pedometer } from '@ionic-native/pedometer/ngx';
import { NgZone } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';

import { CrudService } from './../service/crud.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { firestore } from 'firebase/app';
import { async } from '@angular/core/testing';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';

//for http
import { HttpModule, Http, Headers,Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-scan',
  templateUrl: './scan.page.html',
  styleUrls: ['./scan.page.scss'],
})
export class ScanPage implements OnInit {

  scannedCode = "";  start: boolean; splitted=[]; splitted_0=""; splitted_1=""; splitted_2="";
	PedometerData:any;
  stepCount : any = 0; 
  location: any;
  userEmail=""; userFirstName=""; userLastName=""; userFullName="";
  show_year_month_date=""; device_entry_time: any; device_entry_time_as_number: any;
  phno='';
  constructor(
    private _http: Http,
    public geolocation: Geolocation,
    public toastController: ToastController,
    private ngZoneCtrl: NgZone,
    public platformCtrl: Platform,
    public pedoCtrl: Pedometer,
    private barcodeScanner: BarcodeScanner,
    private storage: Storage,
    private firestore: AngularFirestore,
    private crudService: CrudService,) 
    {
      this.getLocation();
     }

     ngOnInit() {

      this.storage.get('userEmail').then((val) => {
        console.log('Your mail is', val); 
        this.userEmail = val;
      });
  
      this.storage.get('userName').then((val) => {
        console.log('Your F Name is', val);
        this.userFirstName = val;
      });
    
      this.storage.get('userlName').then((val) => {
        console.log('Your L name is', val);
        this.userLastName = val;
      });
   
  
      // var str = "a,b,c";
      // this.splitted= str.split(","); 
      // console.log(this.splitted);
      // console.log(this.splitted[0]);
      // const currDate= new Date().toISOString();
      // alert(currDate);
      const currDate2= new Date();
     // alert(currDate2);
      const estimatedServerTimeMs = new Date().getTime();
      console.log(new Date().getHours());
      console.log(new Date().getMinutes());
      console.log(new Date().getSeconds());
    //  alert(new Date(estimatedServerTimeMs));
      let a1 = (new Date(estimatedServerTimeMs));
      let mnth = a1.getMonth();
      console.log(mnth);
      let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
      var mnth_name = months[mnth];
      console.log(mnth_name);
      let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
      console.log(show_year_month_date); // ok
  
      this.stepCount = 0;
    }

    getLocation() {

      console.log("fetching location");
      this.geolocation.getCurrentPosition().then((resp) => {
        console.log(resp.coords.latitude);
        console.log('Response => ', resp);
        
      }).catch((error) => {
        console.log('Error getting location', error);
      });
  
      // For Live location update
      const watch = this.geolocation.watchPosition();
      watch.subscribe((data: any) => {
        this.location = data.coords;
        console.log(this.location);
       // alert(this.location);
      });
    } 


    getLocationalert() {

      console.log("fetching location");
      this.geolocation.getCurrentPosition().then((resp) => {
        console.log(resp.coords.latitude);
        console.log('Response => ', resp);
        
      }).catch((error) => {
        console.log('Error getting location', error);
      });
  
      // For Live location update
      const watch = this.geolocation.watchPosition();
      watch.subscribe((data: any) => {
        this.location = data.coords;
      //  console.log(this.location);
       // console.log(this.location?.longitude);
       console.log(this.location.latitude);
        console.log(this.location.longitude);
        console.log(this.location.accuracy);
        alert(this.location.latitude);
        alert(this.location.longitude);
        alert(this.location.accuracy);
        
      });
    } 


    scanCode(){
      this.barcodeScanner.scan().then(  
        barcodeData=>{
    this.scannedCode = barcodeData.text;
    
    console.log('Barcode data', barcodeData);
    //alert(JSON.stringify(barcodeData));
   // alert(barcodeData.text)
    const currDate= new Date().toISOString();
    //alert(currDate);
    const currDate2= new Date();
    //alert(currDate2);
    var str = barcodeData.text;
    //alert("str: "+str);
    this.splitted= str.split(","); 
    this.splitted_0 = this.splitted[0];
    this.splitted_1 = this.splitted[1];
    this.splitted_2 = this.splitted[2];
    this.get_device_date_time();
    
    // alert(this.splitted);
    // alert(this.splitted_0);
    // alert(this.splitted_1);
    // alert(this.splitted_2);
        }
      )
    }


    async get_device_date_time(){
      alert("time");
      const currDate2= new Date();
      // alert(currDate2);
       const estimatedServerTimeMs = new Date().getTime();
       console.log(new Date().getHours());
       console.log(new Date().getMinutes());
       console.log(new Date().getSeconds());
       this.device_entry_time = (new Date().getHours() +":"+ new Date().getMinutes() +":"+ new Date().getSeconds() );
       this.device_entry_time_as_number = (new Date().getHours() +""+ new Date().getMinutes() +""+ new Date().getSeconds() );
     //  alert(new Date(estimatedServerTimeMs));
       let a1 = (new Date(estimatedServerTimeMs));
       let mnth = a1.getMonth();
       console.log(mnth);
       let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
       var mnth_name = months[mnth];
       console.log(mnth_name);
       let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
       console.log(show_year_month_date); // ok
       this.show_year_month_date = show_year_month_date;
       await this.saveEntry();
    }


    saveEntry(){
      alert("save");
      let record = {};
      record['location'] = this.splitted_0;
      record['company'] = this.splitted_1;
      record['companyId'] = this.splitted_2;
      record['entryDateserver'] = firebase.firestore.FieldValue.serverTimestamp();
  
      record['useremail'] = this.userEmail;
      record['username'] = this.userFirstName +" "+ this.userLastName;
  
      record['devicedate'] = this.show_year_month_date;
      record['devicetime'] = this.device_entry_time;
      record['devicetimeasnumber'] = this.device_entry_time_as_number;
  
      record['pedometer'] = this.stepCount;
      record['longitude'] = this.location.longitude;
      record['latitude'] = this.location.latitude;
      record['accuracy'] = this.location.accuracy;
  
      this.crudService.create_NewscanLog(record).then(async resp => {
        this.splitted_0="";
        this.splitted_1="";
        this.splitted_2="";
        this.scannedCode="";
        console.log(resp);
      await  this.notificationToast();
      })
        .catch(error => {
          console.log(error);
          alert(error);
        });
  
    }

    async notificationToast() {
      alert("notification");
      const toast = await this.toastController.create({
        header: 'Toast header',
        message: "Scanned Sucessfully !",
        cssClass: "toast-scheme ",
        color: 'warning',
        buttons:[        {
      //    side: 'start',
      //    icon: 'star',
          text: 'Hide',
          handler: () => {
            console.log('Favorite clicked');
          }
        }],
        position: 'top',
        duration: 3000
      });
      toast.present();
      this.call_phpmsg();
    }

    fnGetPedoUpdate(){
      if (this.platformCtrl.is('cordova')) {
        this.pedoCtrl.startPedometerUpdates()
         .subscribe((PedometerData) => {
             this.PedometerData = PedometerData;
             this.ngZoneCtrl.run(() => {
                this.stepCount = this.PedometerData.numberOfSteps;
               // this.startDate = new Date(this.PedometerData.startDate);
               // this.endDate = new Date(this.PedometerData.endDate);
              });
       });
       this.start = true;
       
      // this.fnTost('Please Walk🚶‍to Get Pedometer Update.');
    }else{
    //	this.fnTost('This application needs to be run on📱device');
    console.log("ok");
    }
    }

    fnStopPedoUpdate(){
      this.stepCount=0;
      this.pedoCtrl.stopPedometerUpdates();
      this.start = false;
     
    }

    call_phpmsg()
    {
      this.phpmsg().subscribe(resp =>{
        console.log(resp);
      });
    }


    phpmsg()
    {
      this.phno = '9216142737';
      console.log('inside function');
      let phno = this.phno;
     let msg = "Patrolling Team Rana24x7Services visited your premises on "+ this.device_entry_time;
      let params = '&phno='+this.phno+'&msg='+msg;
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      return this._http.post("https://sapphireprofits.com/sendgrid/" + "send_sms_client.php", params, {headers: headers})
      .map((response: Response) => response.json());
    }

}
