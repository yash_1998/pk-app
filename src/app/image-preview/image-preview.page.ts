import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { CrudService } from '../service/crud.service';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { tap, map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import * as moment from 'moment';
import * as firebase from 'firebase';
@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.page.html',
  styleUrls: ['./image-preview.page.scss'],
})
export class ImagePreviewPage implements OnInit {
  progress: number;
  @Input() base64Url;
  @Input() scanData;
  @Input() userData;
  video: any;
  date = moment();
  fileSize: string;
  uploaded: string;
  uploadTask: any;
  digitalOceanUpload: any;

  constructor(private ref: ChangeDetectorRef,
    public crudService: CrudService,
    public http: HttpClient,
    public cdr: ChangeDetectorRef,
    public modal: ModalController,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.uploadData();
  }

  getDataFormat() {
    return this.date.format('DD/MM/YYYY hh:mm');
  }

  b64toBlob() {
    var myBaseString = this.base64Url;

    // Split the base64 string in data and contentType
    var block = myBaseString.split(";");
    // Get the content type
    var contentType = block[0].split(":")[1];// In this case "image/png"
    // get the real base64 content of the file
    var b64Data = block[1].split(",")[1];// In this case "iVBORw0KGg...."

    // The path where the file will be created
    contentType = contentType || '';
    var sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  uploadData() {
    this.progress = 1;
    // this.crudService.showLoader();
    //Usage example:
    const file = this.b64toBlob();
    console.log(this.base64Url);
    // var file = this.dataURLtoFile(this.base64Url, new Date().getTime() + '.png');
    console.log(file, 'size')
    const tempObject = this.formatFileSize(file.size, 2);
    this.fileSize = tempObject.size;
    if (tempObject.value < 10.00) {
      this.uploadTask = null;
      const formData = new FormData();
      formData.append("file", file);
      formData.append('company_name', this.userData.company_name.replace(/\s/g, ""));
      this.digitalOceanUpload = this.http
        .post("https://us-central1-officeplus-ab43b.cloudfunctions.net/uploadImage", formData, {
          reportProgress: true,
          observe: "events"
        })
        .pipe(
          tap((resp: any) => {
            if (resp && resp.body) {
              if (this.scanData && this.scanData.id) {
                this.createLogUrls(resp.body.url);
              }
            }
          }),
          map((event: any) => {
            if (event.type == HttpEventType.UploadProgress) {

              this.progress = Math.round((100 / event.total) * event.loaded);
              if (event.loaded) {
                this.uploaded = this.formatFileSize(event.loaded, 2).size;
                this.cdr.detectChanges();
              }
            } else if (event.type == HttpEventType.Response) {
              this.progress = null;
            }
          }),
          catchError((err: any) => {
            this.progress = null;
            alert(err.message);
            return throwError(err.message);
          })
        ).subscribe()
    } else {
      this.digitalOceanUpload = null;
      this.fileSize = this.formatFileSize(file.size, 2).size;
      const path = `officeplus/pk/${this.userData.company_name}/`;
      const storageRef = firebase.storage().ref();
      this.uploadTask = storageRef.child(path).put(file);
      this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          // upload in progress
          this.uploaded = this.formatFileSize(snapshot.bytesTransferred, 2);
          this.cdr.detectChanges();
          const percentage = Math.trunc((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
          this.progress = percentage;
        },
        (error) => {
          this.progress = null;
          // upload failed
          console.log(error);

        },
        () => {
          // downloading url
          this.uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
            // return downloadedUrl
            this.progress = null;
            if (this.scanData && this.scanData.id) {
              this.createLogUrls(downloadURL);
            }
          }).catch(error => {
            this.progress = null;
            console.log('error : ' + error);

          });
        }
      );
    }

  }


  createLogUrls(url) {
    this.crudService.createScanLogUrls({ date: moment().valueOf(), url, scanLogId: this.scanData.id }).then(create => {
      this.crudService.showToast('Photo uploaded successfully');
    });
  }


  cancelRequest() {
    this.progress = null;
    if (this.digitalOceanUpload) {
      this.digitalOceanUpload.unsubscribe();
    }
    if (this.uploadTask) {
      this.uploadTask.cancel();
    }
    this.modal.dismiss();
  }


  dataURLtoFile(dataurl, filename): any {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }


  formatFileSize(bytes, decimalPoint) {
    if (bytes == 0) return '0 Bytes';
    var k = 1000,
      dm = decimalPoint || 2,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return { size: parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i], value: parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) } as any;
  }


}
