import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ServiceagentPshiba2Page } from './serviceagent-pshiba2.page';

describe('ServiceagentPshiba2Page', () => {
  let component: ServiceagentPshiba2Page;
  let fixture: ComponentFixture<ServiceagentPshiba2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceagentPshiba2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ServiceagentPshiba2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
