import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServiceagentPshiba2Page } from './serviceagent-pshiba2.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceagentPshiba2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceagentPshiba2PageRoutingModule {}
