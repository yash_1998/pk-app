import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Contacts, Contact, ContactField, ContactName,ContactFindOptions,ContactFieldType } from '@ionic-native/contacts';

//for modal
import { ModalController, AlertController } from '@ionic/angular';
import {CalllogsPage} from '../modals/calllogs/calllogs.page';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  everybody; search_names; search_contacts;
  public goalList: any[];
public loadedGoalList: any[];
 
lunch = "dal"; work_desc=''; clicked_number='';
dinner = "rajma";

  constructor(public callNumber: CallNumber, public contacts: Contacts,public modalController: ModalController) {

    var navigator:any; this.search_names=[]; this.search_contacts='';
    function onSuccess(contacts) {
      alert('Found ' + contacts.length + ' contacts.');
    };
    
    function onError(contactError) {
      alert('onError!');
    };


    // find all contacts with 'Bob' in any name field
     var options      = new ContactFindOptions();
     options.filter   = "aa";
     options.multiple = true;
     options.hasPhoneNumber = true;
   //  options.desiredFields = [navigator.contacts.fieldType.id];
    //  var fields       = ["displayName", "name"];
    // this.search_names =  navigator.contacts.find(fields, onSuccess, onError, options);

  this.search_names = this.contacts.find(["name","displayName"], options);
//  this.everybody = this.contacts.find(["*"]);
   }

  ngOnInit() {
  }

 search(){
  var options      = new ContactFindOptions();
  options.filter   = this.search_contacts;
  options.multiple = true;
  options.hasPhoneNumber = true;
  this.search_names = this.contacts.find(["name","displayName"], options);
 }

 callContact(number: string) {
  this.callNumber.callNumber(number, true)
    .then(() => {
      console.log('Dialer Launched!'),
      this.clicked_number = number;
   //   alert(number);
   //   alert(this.callNumber.callNumber);
      this.openModaWithData();
  })
    .catch(() => console.log('Error launching dialer'));
}

noCall_only_open_modal(number: string) {
      console.log('Dialer Launched!'),
      this.clicked_number = number;
      this.openModaWithData();
}

async openModaWithData() {
 // console.log("inside");
 // console.log(i);

  const modal = await this.modalController.create({
  component: CalllogsPage,
  componentProps: {
  lunch: this.lunch,
  work_desc: this.work_desc,
  clicked_number: this.clicked_number,

  }
});

modal.onWillDismiss().then(dataReturned => {
  //trigger when about to close the modal
  this.dinner = dataReturned.data;
  console. log('Receive: ', this.dinner);
  });

  return await modal.present().then(_ => {
    // triggered when opening the modal
    console.log('Sending: ', this.lunch);

});

}




async openModaDirectly() {
  // console.log("inside");
  // console.log(i);
 
   const modal = await this.modalController.create({
   component: CalllogsPage,
   componentProps: {
   lunch: this.lunch,
  // work_desc: this.work_desc,
  // clicked_number: this.clicked_number,
 
   }
 });
 
 modal.onWillDismiss().then(dataReturned => {
   //trigger when about to close the modal
   this.dinner = dataReturned.data;
   console. log('Receive: ', this.dinner);
   });
 
   return await modal.present().then(_ => {
     // triggered when opening the modal
     console.log('Sending: ', this.lunch);
 
 });
 
 }


}
