import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthenticateService } from '../service/authentication.service';
import { NavController } from '@ionic/angular';
import { CrudService } from './../service/crud.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {


  validations_form: FormGroup;
  errorMessage: string = '';
  successMessage: string = '';
  userId: string;
  userEmail: string;
  usrName: string; usrLastName: string; phno: string;

  validation_messages = {
   'email': [
     { type: 'required', message: 'Email is required.' },
     { type: 'pattern', message: 'Enter a valid email.' }
   ],
   'password': [
     { type: 'required', message: 'Password is required.' },
     { type: 'minlength', message: 'Password must be at least 5 characters long.' }
   ]
 };

  constructor(
    private crudService: CrudService,
    private navCtrl: NavController,
    private authService: AuthenticateService,
    public user:UserService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(){
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }

  tryRegister(value){
    this.authService.registerUser(value)
     .then(res => {
       console.log(res);
       this.errorMessage = "";
       this.successMessage = "Your account has been created. Please log in.";
       this.userId = this.authService.userDetails().uid;
       this.userEmail = this.authService.userDetails().email;
       console.log(this.userId);
       console.log(this.userEmail);
       console.log(this.usrName);
       this.CreateRecord();
     }, err => {
       console.log(err);
       this.errorMessage = err.message;
       this.successMessage = "";
     })
  }

  CreateRecord() {
     let record = {};
     this.user.registered_username = this.usrName;
     record['personalEmail'] = this.userEmail;
     record['Name'] = this.usrName;
     record['lname'] = this.usrLastName;
     record['authId'] = this.userId;
     record['phone'] = this.phno;
     this.crudService.create_NewStudent(record).then(resp => {
    //   this.studentName = "";
    //   this.studentAge = undefined;
    //   this.studentAddress = "";
    //   this.authId="";
       console.log(resp);
     })
       .catch(error => {
         console.log(error);
       });
  }

  goLoginPage(){
    this.navCtrl.navigateBack('');
  }


}

