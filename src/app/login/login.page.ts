import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AlertController, NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import * as firebase from 'firebase';
import { AuthenticateService } from '../service/authentication.service';
import { Storage } from '@ionic/storage';
import { commonDocId } from '../constants';
import * as moment from 'moment';
import { CrudService } from '../service/crud.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  validations_form: FormGroup;
  errorMessage: string = '';

  constructor(
    private alertController : AlertController,
    private navCtrl: NavController,
    private crudService : CrudService,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController,
    private storage: Storage

  ) { 
    
  }

  ngOnInit() {
    this.menuCtrl.enable(false);
    this.storage.get('loggedInUser').then((val) => {
      console.log('L-Your user ID is', val);
      if (val != null||""){
        console.log("L-some one is logged in");
        firebase.firestore().collection('master').doc(commonDocId).collection('Students').doc(val).get().then(document => {
          console.log(document.data());
          const userObject = document.data();
          firebase.firestore().collection('master').doc(commonDocId).collection('activeproducts').where('userId','==', userObject.adminUserId ? userObject.adminUserId: userObject.authId).where('key' ,'==','PK').get().then(products =>{
            let productData = {} as any;
            products.forEach(document => {
             productData =  document.data()
            })
            if(!productData.userId) {
               this.presentAlert('opps!' , 'Please purchase this product');
               return true;
            }else if(moment().valueOf() > moment(productData.end_date).valueOf()){
             this.presentAlert('opps!' , 'Your product has been expired please renew.');
             return true;
            } else {
                 this.navCtrl.navigateForward('/dashboard');
            }
          }).catch(error => {
         
          })
        })
      //  this.navCtrl.navigateRoot('/dashboard');
      }else{
        console.log("L-no one is logged in");
      }
    });

    

    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }


  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ]
  };
 

  loginUser(value){

    // this.storage.get('loggedInUser').then((val) => {
    //   console.log('Your user ID is', val);
    // });

    this.authService.loginUser(value)
    .then(res => {
      console.log(res);
      this.errorMessage = "";
    //this.storage.set('loggedInUser', this.userId); do it
    //this.storage.set('userEmail', this.userEmail); do it
    if (res && res.user && res.user.emailVerified) {
     
      firebase.firestore().collection('master').doc(commonDocId).collection('Students').doc(res.user.uid).get().then(document => {
       console.log(document.data());
       const userObject = document.data();
       debugger
       firebase.firestore().collection('master').doc(commonDocId).collection('activeproducts').where('userId','==', userObject.adminUserId ? userObject.adminUserId: userObject.authId).where('key' ,'==','PK').get().then(products =>{
         let productData = {} as any;
         products.forEach(document => {
          productData =  document.data()
         })
         if(!productData.userId) {
            this.presentAlert('opps!' , 'Please purchase this product');
            return true;
         }else if(moment().valueOf() > moment(productData.end_date).valueOf()){
          this.presentAlert('opps!' , 'Your product has been expired please renew.');
          return true;
         } else {
          this.storage.set('userEmail' , res.user.email);
          this.storage.set('userData' ,document.data() ).then(resp => {
            this.storage.set('loggedInUser',res.user.uid).then(resp => {
              this.navCtrl.navigateForward('/dashboard');
            });
          })
         }

       }).catch(error => {
      
       })
      
       
      })
     
     
    } else {
      this.crudService.showToast('Email is not verified')
    }
    }, err => {
      this.errorMessage = err.message;
    })
  }

  goToRegisterPage(){
    this.navCtrl.navigateForward('/register');
  }


  async presentAlert(title ,message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: title,
      message,
      buttons: ['OK']
    });

    await alert.present();

    const { role } = await alert.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  


}
