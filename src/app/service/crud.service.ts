import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { LoadingController, ToastController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { commonDocId } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class CrudService {


  constructor(
    private firestore: AngularFirestore,
    public http: HttpClient,
    public toastController: ToastController,
    public loadingController: LoadingController
  ) { }


  create_NewLog(record) {
    return this.firestore.collection('logs').add(record);
  }

  showLoader() {

    this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();
    });

  }

  updateNewscanLog(id, record) {
    return this.firestore.collection('pk').doc(commonDocId).collection('gscanLog').doc(id).update(record);
  }

  getScanData(id) {
    return this.firestore.collection('pk').doc(commonDocId).collection('gscanLog').doc(id).get();
  }


  createScanLogUrls(record) {
    return this.firestore.collection('pk').doc(commonDocId).collection('gscanLogurl').add(record);
  }


  uploadImage(data) {
    return this.http.post('https://us-central1-officeplus-ab43b.cloudfunctions.net/uploadAttendancePhoto', data).pipe(
      map((response: any) => response));
  }

  // Hide the loader if already created otherwise return error
  hideLoader() {

    this.loadingController.dismiss().then((res) => {
      console.log('Loading dismissed!', res);
    }).catch((error) => {
      console.log('error', error);
    });

  }

  create_NewscanLog(record) {
    return this.firestore.collection('pk').doc(commonDocId).collection('gscanLog').add(record);
  }
  create_NewsMarkAttendance(record) {
    return this.firestore.collection('pk').doc(commonDocId).collection('attendance').add(record);
  }

  update_NewsMarkAttendance(id, record) {
    return this.firestore.collection('pk').doc(commonDocId).collection('attendance').doc(id).update(record);
  }

  create_NewStudent(record) {
    return this.firestore.collection('Students').add(record);
  }

  create_NewCalllog(record) {
    return this.firestore.collection('calllogs').add(record);
  }

  create_NewContact(record) {
    return this.firestore.collection('logs').add(record);
  }

  async showToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 5000
    }).then((toastData) => {
      toastData.present();
    });
  }



  read_Students() {
    return this.firestore.collection('Students').snapshotChanges();
  }



  read_ctglist() {
    return this.firestore.collection('complaintctg').snapshotChanges();
  }





  update_Student(recordID, record) {
    this.firestore.doc('Students/' + recordID).update(record);
  }



  delete_Student(record_id) {
    this.firestore.doc('Students/' + record_id).delete();
  }





}
