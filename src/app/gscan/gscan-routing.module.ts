import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GscanPage } from './gscan.page';

const routes: Routes = [
  {
    path: '',
    component: GscanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GscanPageRoutingModule {}
