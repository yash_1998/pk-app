import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GscanPageRoutingModule } from './gscan-routing.module';

import { GscanPage } from './gscan.page';
import { ImagePreviewPageModule } from '../image-preview/image-preview.module';
import { VidePreviewPageModule } from '../vide-preview/vide-preview.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GscanPageRoutingModule,
    ImagePreviewPageModule,
    VidePreviewPageModule
  ],
  declarations: [GscanPage]
})
export class GscanPageModule { }
