import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Pedometer } from '@ionic-native/pedometer/ngx';
import { NgZone } from '@angular/core';
import { Platform, ToastController, ModalController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { CrudService } from './../service/crud.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { File as CustomFile } from '@ionic-native/file/ngx';
import { VideoCapturePlus, VideoCapturePlusOptions, MediaFile } from '@ionic-native/video-capture-plus/ngx';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { commonDocId } from '../constants';
import * as moment from 'moment';
import { ImagePreviewPage } from '../image-preview/image-preview.page';
import { VidePreviewPage } from '../vide-preview/vide-preview.page';

@Component({
  selector: 'app-gscan',
  templateUrl: './gscan.page.html',
  styleUrls: ['./gscan.page.scss'],
})
export class GscanPage implements OnInit {
  scanData = {} as any;
  date = moment();
  base64Url = '';
  scannedCode = ""; start: boolean; splitted = []; splitted_0 = ""; splitted_1 = ""; splitted_2 = "";
  PedometerData: any;
  selectedFile: any;
  stepCount: any = 0; showscan = false;
  location: any = null;
  userEmail = ""; userFirstName = ""; userLastName = ""; userFullName = "";
  show_year_month_date = ""; device_entry_time: any; device_entry_time_as_number: any;
  longlat_null = false;
  userData = {} as any;

  constructor(
    public geolocation: Geolocation,
    public toastController: ToastController,
    private ngZoneCtrl: NgZone,
    public cdr: ChangeDetectorRef,
    public platformCtrl: Platform,
    public modalController: ModalController,
    public pedoCtrl: Pedometer,
    public file: CustomFile,
    private barcodeScanner: BarcodeScanner,
    private storage: Storage,
    private videoCapturePlus: VideoCapturePlus,
    private camera: Camera,
    private platform: Platform,
    private firestore: AngularFirestore,
    private crudService: CrudService, ) {

  }

  ngOnInit() {
    this.platform.ready().then(resp => {
      this.getLocation();
    })

    this.storage.get('userEmail').then((val) => {
      console.log('Your mail is', val);
      this.userEmail = val;
    });

    this.storage.get('userData').then((val) => {
      console.log('Your mail is', val);
      this.userData = val;
    });

    this.storage.get('userName').then((val) => {
      console.log('Your F Name is', val);
      this.userFirstName = val;
    });

    this.storage.get('userlName').then((val) => {
      console.log('Your L name is', val);
      this.userLastName = val;
    });


    // var str = "a,b,c";
    // this.splitted= str.split(","); 
    // console.log(this.splitted);
    // console.log(this.splitted[0]);
    // const currDate= new Date().toISOString();
    // alert(currDate);
    const currDate2 = new Date();
    // alert(currDate2);
    const estimatedServerTimeMs = new Date().getTime();
    console.log(new Date().getHours());
    console.log(new Date().getMinutes());
    console.log(new Date().getSeconds());
    //  alert(new Date(estimatedServerTimeMs));
    let a1 = (new Date(estimatedServerTimeMs));
    let mnth = a1.getMonth();
    console.log(mnth);
    let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    var mnth_name = months[mnth];
    console.log(mnth_name);
    let show_year_month_date = a1.getDate() + '-' + mnth_name + '-' + a1.getFullYear();
    console.log(show_year_month_date); // ok

    this.stepCount = 0;
  }

  async takeVideo() {
    const options: VideoCapturePlusOptions = {
      limit: 1,
      highquality: true,
      duration: 60,
    }

    this.videoCapturePlus.captureVideo(options).then(async (mediafile: MediaFile[]) => {
      const f = mediafile[0]
      const path = f.fullPath.substr(0, f.fullPath.lastIndexOf('/') + 1);
      const type = this.getMimeType(f.name.split('.').pop());
      const buffer = await this.file.readAsArrayBuffer(path, f.name);
      const fileBlob = new Blob([buffer], type);
      const modal = await this.modalController.create({
        component: VidePreviewPage,
        componentProps: { videoFile: fileBlob, scanData: this.scanData, userData: this.userData },
        cssClass: 'my-custom-class'
      });
      await modal.present();
    }, error => console.log('Something went wrong'));
  }

  // onFileSelect(event) {
  //   this.selectedFile = event.target.files[0];
  // }

  getMimeType(fileExt) {
    if (fileExt == 'wav') return { type: 'audio/wav' };
    else if (fileExt == 'jpg') return { type: 'image/jpg' };
    else if (fileExt == 'mp4') return { type: 'video/mp4' };
    else if (fileExt == 'MOV') return { type: 'video/quicktime' };
  }

  getLocation() {

    console.log("fetching location");
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true, maximumAge: 3600, timeout: 10000 }).then((resp) => {
      console.log(resp.coords.latitude);
      this.ngZoneCtrl.run(() => {
        this.location = resp.coords;
        this.cdr.detectChanges();
      });

      console.log('Response => ', resp);

    }).catch((error) => {
      console.log('Error getting location', error);
    });

    // For Live location update
    // const watch = this.geolocation.watchPosition();
    // watch.subscribe((data: any) => {
    //   this.ngZoneCtrl.run(() => {
    //     this.location = data.coords;
    //     this.cdr.detectChanges();
    //   });
    //   console.log(this.location);
    //  // alert(this.location);
    // });
  }

  getLocationalert() {

    console.log("fetching location");
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords.latitude);
      console.log('Response => ', resp);

    }).catch((error) => {
      console.log('Error getting location', error);
    });

    // For Live location update
    const watch = this.geolocation.watchPosition();
    watch.subscribe((data: any) => {
      this.location = data.coords;
      //  console.log(this.location);
      // console.log(this.location?.longitude);
      console.log(this.location.latitude);
      console.log(this.location.longitude);
      console.log(this.location.accuracy);
      alert(this.location.latitude);
      alert(this.location.longitude);
      alert(this.location.accuracy);

    });
  }

  scanCode() {
    this.splitted_0 = "";
    this.splitted_1 = "";
    this.splitted_2 = "";
    this.barcodeScanner.scan().then(
      barcodeData => {
        this.scannedCode = barcodeData.text;

        console.log('Barcode data', barcodeData);
        //alert(JSON.stringify(barcodeData));
        // alert(barcodeData.text)
        const currDate = new Date().toISOString();
        //alert(currDate);
        const currDate2 = new Date();
        //alert(currDate2);
        var str = barcodeData.text;
        //alert("str: "+str);
        this.splitted = str.split(",");
        this.splitted_0 = this.splitted[0];
        this.splitted_1 = this.splitted[1];
        this.splitted_2 = this.splitted[2];
        this.get_device_date_time();

        // alert(this.splitted);
        // alert(this.splitted_0);
        // alert(this.splitted_1);
        // alert(this.splitted_2);
      }
    )
  }

  scanCodetest() {
    //save test
    this.splitted_1 = "Royal Riders";
    // this.show_year_month_date = "9-SEP-2020";
    // this.device_entry_time = "18:27:18";
    // this.device_entry_time_as_number = 63;
    //save test
    this.get_device_date_time();
  }

  async get_device_date_time() {
    console.log("start time cal")
    const currDate2 = new Date();
    // alert(currDate2);
    const estimatedServerTimeMs = new Date().getTime();
    console.log(new Date().getHours());
    console.log(new Date().getMinutes());
    console.log(new Date().getSeconds());
    this.device_entry_time = (new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
    this.device_entry_time_as_number = (new Date().getHours() + "" + new Date().getMinutes() + "" + new Date().getSeconds());
    //  alert(new Date(estimatedServerTimeMs));
    let a1 = (new Date(estimatedServerTimeMs));
    let mnth = a1.getMonth();
    console.log(mnth);
    let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    var mnth_name = months[mnth];
    console.log(mnth_name);
    let show_year_month_date = a1.getDate() + '-' + mnth_name + '-' + a1.getFullYear();
    console.log(show_year_month_date); // ok
    this.show_year_month_date = show_year_month_date;
    await this.saveEntry();
  }

  saveEntry() {
    console.log("end time cal")
    let record = {};

    if (this.location == null) {
      //   alert("no long lat");
      record['longitude'] = null;
      record['latitude'] = null;
      record['accuracy'] = null;
      this.longlat_null = true;
      // this.notificationToast_no_lat_long();
    }
    else {
      this.longlat_null = false;
      //  alert("long lat found");
      record['longitude'] = this.location.longitude;
      record['latitude'] = this.location.latitude;
      record['accuracy'] = this.location.accuracy;
    }

    record['location'] = this.splitted_0;
    record['company'] = this.splitted_1;
    record['companyId'] = this.splitted_2;
    record['entryDateserver'] = firebase.firestore.FieldValue.serverTimestamp();

    record['useremail'] = this.userEmail;
    record['username'] = this.userFirstName + " " + this.userLastName;

    record['devicedate'] = this.show_year_month_date;
    record['devicetime'] = this.device_entry_time;
    record['devicetimeasnumber'] = this.device_entry_time_as_number;

    record['pedometer'] = this.stepCount;
    // record['longitude'] = this.location.longitude;
    // record['latitude'] = this.location.latitude;
    // record['accuracy'] = this.location.accuracy;
    record['company_code'] = this.userData.company_code ? this.userData.company_code : '';
    this.crudService.create_NewscanLog(record).then(async resp => {
      this.scanData = resp;
      console.log(resp);
      this.serial_total_func();

      // this.splitted_0="";
      // this.splitted_1="";
      // this.splitted_2="";
      this.scannedCode = "";
    })
      .catch(error => {
        console.log(error);
      });

  }

  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.base64Url = base64Image;
      this.date = moment();
      this.presentImageModal();
      // this.uploadData();
    }, (err) => {
      // Handle error
    });
  }


  async notificationToast() {
    const toast = await this.toastController.create({
      header: 'Alert',
      message: "Scanned & Saved Sucessfully !",
      cssClass: "toast-scheme ",
      color: 'warning',
      buttons: [{
        //    side: 'start',
        //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'top',
      duration: 3000
    });
    toast.present();
  }

  async notificationToast_no_lat_long() {
    const toast = await this.toastController.create({
      header: 'Alert: Location not found',
      message: "Please enable GPS. Close app and open again. Scanned & Saved Sucessfully !",
      cssClass: "toast-scheme ",
      color: 'warning',
      buttons: [{
        //    side: 'start',
        //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'middle',
      duration: 5000
    });
    toast.present();
  }

  async presentImageModal() {
    const modal = await this.modalController.create({
      component: ImagePreviewPage,
      componentProps: { scanData: this.scanData, userData: this.userData, base64Url: this.base64Url },
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }


  fnGetPedoUpdate() {
    if (this.platformCtrl.is('cordova')) {
      this.pedoCtrl.startPedometerUpdates()
        .subscribe((PedometerData) => {
          this.PedometerData = PedometerData;
          this.ngZoneCtrl.run(() => {
            this.stepCount = this.PedometerData.numberOfSteps;
            // this.startDate = new Date(this.PedometerData.startDate);
            // this.endDate = new Date(this.PedometerData.endDate);
          });
        });
      this.start = true;
      this.showscan = true;
      // this.fnTost('Please Walk🚶‍to Get Pedometer Update.');
    } else {
      //	this.fnTost('This application needs to be run on📱device');
      console.log("ok");
    }
  }

  fnStopPedoUpdate() {
    this.stepCount = 0;
    this.pedoCtrl.stopPedometerUpdates();
    this.start = false;
    this.showscan = false;
  }

  serial_total_func() {
    //1. check if the doc exists
    var docRef = firebase.firestore().collection('pk').doc(commonDocId).collection('scan_total_dtwise').doc(this.show_year_month_date);
    docRef.get().then(doc => {
      if (doc.exists) {
        console.log("Document data:", doc.data());
        this.update_exisiting_serial_total();
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
        this.create_doc();
      }
    }).catch(function (error) {
      console.log("Error getting document:", error);
    });
  }


  update_exisiting_serial_total() {
    var docRef = firebase.firestore().collection('pk').doc(commonDocId).collection('scan_total_dtwise').doc(this.show_year_month_date);
    firebase.firestore().runTransaction(transaction => {
      return transaction.get(docRef).then(incDoc => {
        //if no value exist, assume it as 0 and increase to 1
        var newIncId = (incDoc.data()[this.splitted_1] || 0) + 1;
        transaction.update(docRef, { [this.splitted_1]: newIncId });
        return newIncId;
      });
    }).then(async  newIncId => {
      if (this.longlat_null == false) {
        await this.notificationToast();
      }
      if (this.longlat_null == true) {
        await this.notificationToast_no_lat_long();
      }
      console.log("updated serial number total:  ", newIncId);
      this.splitted_1 = "";
      // await  this.CreateNewOrder();
    }).catch(function (err) {
      // Catch block to get any error
      console.error(err);
    });

  }

  create_doc() {
    firebase.firestore().collection('pk').doc(commonDocId).collection("scan_total_dtwise").doc(this.show_year_month_date).set({
      [this.splitted_1]: 1,
      company_code: this.userData.company_code ? this.userData.company_code : ''
      //number: 1
    })
      .then(async x => {
        if (this.longlat_null == false) {
          await this.notificationToast();
        }
        if (this.longlat_null == true) {
          await this.notificationToast_no_lat_long();
        }
        console.log("Document created successfully with number 1 !");
        this.splitted_1 = "";
      })
      .catch(error => {
        console.error("Error writing document: ", error);
      });

  }

}
