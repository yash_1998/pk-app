import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GscanPage } from './gscan.page';

describe('GscanPage', () => {
  let component: GscanPage;
  let fixture: ComponentFixture<GscanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GscanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GscanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
