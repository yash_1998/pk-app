import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';


import { CallLog, CallLogObject } from '@ionic-native/call-log/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';

import { CrudService } from './../service/crud.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { firestore } from 'firebase/app';
import { async } from '@angular/core/testing';
import { Storage } from '@ionic/storage';
 
@Component({
  selector: 'app-logs',
  templateUrl: './logs.page.html',
  styleUrls: ['./logs.page.scss'],
})
export class LogsPage implements OnInit {

  filters: CallLogObject[];filters_o: CallLogObject[];
  recordsFound: any; recordsFound_o: any;
  recordsFoundText: string; recordsFoundText_o: string;
  listTyle:string;
  today_or_yesterday='';

  epoch_val = 1590645517326;
show_yr_mnth_dt; show_yr_mnth_dt2;
  from_time: any;
  from_time_o_y: any;
  from_time_o_t: any;
  userEmail: string;
  userId: string;
  userNm: string ='';userlNm: string ='';userFullname=''; userAuthid='';
  incoming_synced='';outgoing_synced='';missed_synced='';

  constructor(
    private storage: Storage,
    private firestore: AngularFirestore,
    private crudService: CrudService,
    private callLog: CallLog, 
    private callNumber: CallNumber,
    private platform: Platform
  ) {
 
  //   this.epoch_val = 1590645517326;
  //   let a1 = (new Date(this.epoch_val)); // converted to date time lambi string
  //  let mnth = a1.getMonth();
  //             console.log(mnth);
  //             let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
  //             var mnth_name = months[mnth];
  //             console.log(mnth_name);
  //             let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
  //             console.log(show_year_month_date);


    this.platform.ready().then(() => {

      this.callLog.hasReadPermission().then(hasPermission => {
        if (!hasPermission) {
          this.callLog.requestReadPermission().then(results => {
          //  this.getContacts("type","1","==");
          })
            .catch(e => alert(" requestReadPermission " + JSON.stringify(e)));
        } else {
      //    this.getContacts("type", "1", "==");
        }
      })
        .catch(e => alert(" hasReadPermission " + JSON.stringify(e)));
    });
   }

  ngOnInit() {

    //date to check in server
var today = new Date();
today.setHours(9, 30, 0);
var yesterday = new Date(today);
yesterday.setDate(today.getDate());
var fromTime = yesterday.getTime();
this.from_time = yesterday.getTime();
this.epoch_val = this.from_time;
let a1 = (new Date(this.epoch_val)); // converted to date time lambi string
let mnth = a1.getMonth();
          console.log(mnth);
          let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
          var mnth_name = months[mnth];
          console.log(mnth_name);
          let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
          console.log(show_year_month_date);
this.show_yr_mnth_dt2 = show_year_month_date;

    this.storage.get('userEmail').then((val) => {
      console.log('Your mail is', val); 
      this.userEmail = val;
    });
    this.storage.get('userName').then((val) => {
      console.log('Your F Name is', val);
      this.userNm = val;
    });
  
    this.storage.get('userlName').then((val) => {
      console.log('Your L name is', val);
      this.userlNm = val;
      this.userFullname = this.userNm +' '+ this.userlNm;
      console.log(this.userFullname);
    });

    this.storage.get('loggedInUser').then((val) => {
      console.log('User auth id is', val);
      this.userAuthid = val;
    });
  }

  getContacts(name, value, operator) {
    if(value == '1'){
      this.listTyle = "Incoming";
    }else if(value == '2'){
      this.listTyle = "Outgoing";
    }else if(value == '3'){
      this.listTyle = "Missed";
    }

    //Getting Yesterday Time
    var today = new Date();
    today.setHours(9, 30, 0);
    var yesterday = new Date(today);
    yesterday.setDate(today.getDate());
    var fromTime = yesterday.getTime();
    this.from_time = yesterday.getTime();

    //Getting Today time
    // var today_s = new Date();
    // today_s.setHours(9, 30, 0);
    // //var today2 = new Date(today);
    // var today2_s = today_s.getTime();
    // var fromTime = today2_s;
    // this.from_time = today2_s;
    // console.log(today_s);
    // console.log(today2_s);
  
  //  alert(fromTime.toString());
    this.filters = [{
      name: name,
      value: value,
      operator: operator,
    }, {
      name: "date",
      value: fromTime.toString(),
      operator: ">=",
    },
    // {
    //   name: "duration",
    //   value: value,
    //   operator: operator,
    // }
  
  
  ];
    this.callLog.getCallLog(this.filters)
      .then(results => {
        this.recordsFoundText = JSON.stringify(results);
    //    console.log(this.recordsFoundText);
        this.recordsFound = results;//JSON.stringify(results);
   //     console.log(this.recordsFound);
      })
      .catch(e => alert(" LOG " + JSON.stringify(e)));
  }

//    getContacts_outgoing(name,value, operator) {
//   // let value;
//   //  for (value = 1; value <= 3; value++) {
//  console.log(value);
//  alert(value);

//     //Getting Yesterday Time
//     var today = new Date();
//     today.setHours(9, 30, 0);
//     var yesterday = new Date(today);
//     yesterday.setDate(today.getDate() - 1);
//     var fromTime_o_y = yesterday.getTime();
//     this.from_time_o_y = yesterday.getTime();

//     //Getting Today time
//     var today_s = new Date();
//     today_s.setHours(9, 30, 0);
//     //var today2 = new Date(today);
//     var today2_s = today_s.getTime();
//     var fromTime_o_t = today2_s;
//     this.from_time_o_t = today2_s;
//     console.log(today_s);
//     console.log(today2_s);
  
//     if(this.today_or_yesterday == "y"){
//       console.log("point 1");
// alert(this.from_time_o_y);
// alert("inside y");
// alert(fromTime_o_y.toString());
//     this.filters_o = [{
//       name: name,
//       value: value,
//       operator: operator,
//     }, {
//       name: "date",
//       value: fromTime_o_y.toString(),
//       operator: ">=",
//     },
//   ];
// }

// if(this.today_or_yesterday == "t"){
//   console.log("point 1");
//   alert(this.from_time_o_t);
//   alert("inside t");
//   this.filters_o = [{
//     name: name,
//     value: value,
//     operator: operator,
//   }, {
//     name: "date",
//     value: fromTime_o_t.toString(),
//     operator: ">=",
//   },
// ];
// }





//  setTimeout(()=>{
//     this.callLog.getCallLog(this.filters_o)
//       .then(results_o => {
//         this.recordsFoundText_o = JSON.stringify(results_o);
//     //    console.log(this.recordsFoundText);
//         this.recordsFound_o = results_o;//JSON.stringify(results);

//         alert(this.recordsFound_o);
//         alert(this.recordsFoundText_o);
//         console.log("point 2");
//       })
//       .catch(e => alert(" LOG " + JSON.stringify(e)));
//     }, 1000);

//       let record = {};
//       record['name'] = "saurabh";

//       if(value==1){
//         record['type'] = "in";
//         console.log("point 3");
//         }

//       if(value==2){
//       record['type'] = "out";
//       console.log("point 3");
//       }

//       if(value==3){
//         record['type'] = "missed";
//         console.log("point 3");
//         }
  
//   //changing epoch value to readable format, 28-MAY-2020
//   if(this.today_or_yesterday=="y"){
//     this.epoch_val = this.from_time_o_y;
//     alert(this.epoch_val);
//     console.log("point 4");
//   }
//   if(this.today_or_yesterday=="t"){
//     this.epoch_val = this.from_time_o_t;
//     alert(this.epoch_val);
//     console.log("point 4");
//   }
      
//       let a1 = (new Date(this.epoch_val)); // converted to date time lambi string
//       alert(a1);
//      let mnth = a1.getMonth();
//                 console.log(mnth);
//                 let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
//                 var mnth_name = months[mnth];
//                 console.log(mnth_name);
//                 let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
//                 console.log(show_year_month_date);
  
//       record['date'] = show_year_month_date;
//       record['outgoing'] = this.recordsFound_o;
//      // record['recordstext'] = this.recordsFoundText;
  
//      setTimeout(()=>{
//     console.log("point 5");
//        console.log("inside saving");
//     this.crudService.create_NewLog(record).then(resp => {
//       console.log("point 6");
//         console.log(resp,resp.id);
//         if(resp.id != null){
//         console.log("saved successfully");
//         alert("saved successfully");
//       //  this.recordsFound_o='';
//       //  this.recordsFoundText_o='';

//         }
      
//       })
//         .catch(error => {
//           console.log(error);
//           console.log("point 6");
//           return;
//         });
//       }, 1000);
// console.log("loop till here");
//    // } // ending for loop
//   }



  callThisNumber(number){
    this.callNumber.callNumber(number, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  

//   Sync_today_incoming(i){
//     // alert(i);
//        this.today_or_yesterday = i;
//        alert(this.today_or_yesterday+"sync func ini");
//        this.getContacts_outgoing('type',1,'==');
//      }

//   Sync_today_outgoing(i){
//  // alert(i);
//     this.today_or_yesterday = i;
//     alert(this.today_or_yesterday+"sync func ini");
//     this.getContacts_outgoing('type',2,'==');
//   }

  sendLogs(){

    let record = {};
   // record['name'] = "saurabh";

    if(this.listTyle == "Incoming"){
      record['type'] = "Incoming";
      this.incoming_synced="Incoming synced";
    }

    if(this.listTyle == "Outgoing"){
      record['type'] = "Outgoing";
      this.outgoing_synced = "Outgoing synced";
    }

    if(this.listTyle == "Missed"){
      record['type'] = "Missed";
      this.missed_synced = "Missed synced";
    }


//changing epoch value to readable format, 28-MAY-2020
  //   this.epoch_val = this.from_time;
  //   let a1 = (new Date(this.epoch_val)); // converted to date time lambi string
  //  let mnth = a1.getMonth();
  //             console.log(mnth);
  //             let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
  //             var mnth_name = months[mnth];
  //             console.log(mnth_name);
  //             let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
  //             console.log(show_year_month_date);
  //             this.show_yr_mnth_dt = show_year_month_date;

    record['date'] = this.show_yr_mnth_dt2;
    record['logs'] = this.recordsFound;

    record['fullname'] = this.userFullname;
    record['useremail'] = this.userEmail;
    record['authid'] = this.userAuthid;
    record['version'] = "v1.3";
   // record['recordstext'] = this.recordsFoundText;

    this.crudService.create_NewLog(record).then(resp => {
      console.log(resp,resp.id);
      if(resp.id != null){
      console.log("saved successfully");
      alert("saved successfully");
      }
    
    })
      .catch(error => {
        console.log(error);
      });

  }
 
  check(){



    //where condition
// this.firestore.collection('logs' , ref => ref
// .where("type" , "==" , "Missed") 
// .where("date" , "==" , show_year_month_date) 
// )
// .snapshotChanges()
// .subscribe(response => {
// if(response.length){
//   console.log("entry hai");
//   alert("entry hai");
//   return false;
// }
// if(!response.length){
//   console.log("entry nahi hai");
//   alert("entry nahi hai");
//   this.sendLogs();
//  // return false;
// }})

//alert("here");
firebase.firestore().collection('logs')
.where("useremail" , "==" , this.userEmail)
.where("date" , "==" , this.show_yr_mnth_dt2)
.where("type" , "==" , this.listTyle)
.get()
.then(querySnapshot => {

 querySnapshot.forEach(doc=> {
 console.log(doc.id, " => ", doc.data());
 //alert(querySnapshot.docs.length);
 console.log(querySnapshot.docs.length);
});

 if(querySnapshot.docs.length==null){
  console.log("its null");
}
 if(querySnapshot.docs.length>0){
   console.log("entry found");
 //  alert(this.userEmail+this.show_yr_mnth_dt2+this.listTyle);
   alert(this.listTyle +" entry for " + this.show_yr_mnth_dt2 +" already exists, deleting it and saving again.");
   this.delete();
 }
 if(!querySnapshot.docs.length){
   console.log("no entry");
  // alert("no entry");
   this.sendLogs();
 }

})
.catch(function(error) {
   console.log("Error getting documents: ", error);
   alert("error state");
})

  }

delete(){
 // alert(this.userEmail+this.show_yr_mnth_dt+this.listTyle);
  firebase.firestore().collection('logs')
  .where("useremail" , "==" , this.userEmail)
  .where("date" , "==" , this.show_yr_mnth_dt2)
  .where("type" , "==" , this.listTyle)

.get().
then(function(querySnapshot) {
  querySnapshot.forEach(function(doc) {
    doc.ref.delete();
    
    alert("delete successful");
  });
});
setTimeout(()=>{
this.sendLogs();
},1000);
}

}