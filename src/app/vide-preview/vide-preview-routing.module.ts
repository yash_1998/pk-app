import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VidePreviewPage } from './vide-preview.page';

const routes: Routes = [
  {
    path: '',
    component: VidePreviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VidePreviewPageRoutingModule {}
