import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VidePreviewPageRoutingModule } from './vide-preview-routing.module';

import { VidePreviewPage } from './vide-preview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VidePreviewPageRoutingModule
  ],
  declarations: [VidePreviewPage],
  entryComponents: [VidePreviewPage]
})
export class VidePreviewPageModule { }
