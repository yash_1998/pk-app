import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VidePreviewPage } from './vide-preview.page';

describe('VidePreviewPage', () => {
  let component: VidePreviewPage;
  let fixture: ComponentFixture<VidePreviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VidePreviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VidePreviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
