import { Component, OnInit, Input, ViewChild, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { CrudService } from '../service/crud.service';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-vide-preview',
  templateUrl: './vide-preview.page.html',
  styleUrls: ['./vide-preview.page.scss'],
})
export class VidePreviewPage implements OnInit, AfterViewInit {
  progress: number;
  @Input() scanData;
  @ViewChild('myvideo') myVideo: any;
  @Input() userData;
  @Input() videoFile;
  video: any;
  date = moment();
  base64Url = '';
  fileSize: string;
  uploaded: string;
  uploadTask: any;
  constructor(private ref: ChangeDetectorRef,
    public crudService: CrudService,
    public http: HttpClient,
    private storage: Storage,
    public cdr: ChangeDetectorRef,
    public modal: ModalController,
  ) { }

  ngOnInit() {
    this.uploadData();
  }

  getDataFormat() {
    return this.date.format('DD/MM/YYYY hh:mm');
  }

  ngAfterViewInit() {
    var URL = window.URL || window.webkitURL;
    let video = this.myVideo.nativeElement;
    var url = URL.createObjectURL(this.videoFile);
    video.src = url;
    video.load();
  }

  cancelRequest() {
    this.progress = null;
    if (this.uploadTask) {
      this.uploadTask.cancel();
    }
    this.modal.dismiss();
  }

  uploadData() {
    this.progress = 1;
    // this.crudService.showLoader();
    //Usage example:
    console.log(this.videoFile, 'video file')
    this.fileSize = this.formatFileSize(this.videoFile.size, 2);
    const path = `officeplus/pk/${this.userData.company_name}/`;
    const storageRef = firebase.storage().ref();
    this.uploadTask = storageRef.child(path).put(this.videoFile);
    this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: any) => {
        // upload in progress
        this.uploaded = this.formatFileSize(snapshot.bytesTransferred, 2);

        const percentage = Math.trunc((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        this.progress = percentage;
        this.cdr.detectChanges();
      },
      (error) => {
        console.log(error, 'error 2');
        this.progress = null;
        // upload failed
        console.log(error);

      },
      () => {
        // downloading url
        this.uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
          // return downloadedUrl
          this.progress = null;
          if (this.scanData && this.scanData.id) {
            this.crudService.createScanLogUrls({ date: moment().valueOf(), url: downloadURL, scanLogId: this.scanData.id }).then(create => {
              this.crudService.showToast('Video uploaded successfully');
            });
          }
        }).catch(error => {
          this.progress = null;
          console.log('error : ' + error);

        });
      }
    );

    // const formData = new FormData();
    // formData.append("file", this.videoFile);
    // formData.append('company_name', this.userData.company_name.replace(/\s/g, ""));
    // this.http
    //   .post("https://us-central1-officeplus-ab43b.cloudfunctions.net/uploadImage", formData, {
    //     reportProgress: true,
    //     observe: "events"
    //   })
    //   .pipe(
    //     tap((resp: any) => {
    //       if (resp && resp.body) {
    //         if (this.scanData && this.scanData.id) {
    //           this.crudService.getScanData(this.scanData.id).subscribe(scan => {
    //             const data = scan.data();
    //             data.docId = this.scanData.id;
    //             if (!data.urls) {
    //               data.urls = [];
    //             }
    //             data.urls.push(resp.body.url);
    //             this.crudService.updateNewscanLog(this.scanData.id, data).then(update => {
    //               this.crudService.showToast('Photo uploaded successfully');
    //             })
    //             // this.modal.dismiss();
    //             console.log(resp.data())
    //           })
    //         }
    //       }
    //     }),
    //     map((event: any) => {
    //       if (event.type == HttpEventType.UploadProgress) {

    //         this.progress = Math.round((100 / event.total) * event.loaded);
    //         if (event.loaded) {
    //           this.uploaded = this.formatFileSize(event.loaded, 2);
    //           this.cdr.detectChanges();
    //         }
    //       } else if (event.type == HttpEventType.Response) {
    //         this.progress = null;
    //       }
    //     }),
    //     catchError((err: any) => {
    //       this.progress = null;
    //       alert(err.message);
    //       return throwError(err.message);
    //     })
    //   ).toPromise()

  }


  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }


  formatFileSize(bytes, decimalPoint) {
    if (bytes == 0) return '0 Bytes';
    var k = 1000,
      dm = decimalPoint || 2,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }


}
