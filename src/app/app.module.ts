import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { VideoCapturePlus } from '@ionic-native/video-capture-plus/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { File } from '@ionic-native/file/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Contacts } from '@ionic-native/contacts';
import { CallLog, CallLogObject } from '@ionic-native/call-log/ngx';


import * as firebase from 'firebase';
import { firestore } from 'firebase/app';

//  firebase imports, remove what you don't require
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireMessagingModule } from '@angular/fire/messaging';

//auth login
import { AuthenticateService } from './service/authentication.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';

//geolocation
import { Geolocation } from '@ionic-native/geolocation/ngx';

// environment
import { environment } from '../environments/environment';

//for scanner
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Pedometer } from '@ionic-native/pedometer/ngx';

//for sms
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest, HttpClientModule } from '@angular/common/http';
import { HttpModule, Http } from '@angular/http';

//camera
import { Camera } from '@ionic-native/camera/ngx';
import { WebcamModule } from 'ngx-webcam';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence(), // to enable offline persistance
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireMessagingModule,
    HttpClientModule,
    HttpModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    Camera,
    WebcamModule,
    StatusBar,
    SplashScreen,
    CallNumber,
    Contacts,
    CallLog,
    AuthenticateService,
    Pedometer,
    BarcodeScanner,
    Geolocation,
    VideoCapturePlus,
    File,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
