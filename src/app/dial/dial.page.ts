import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-dial',
  templateUrl: './dial.page.html',
  styleUrls: ['./dial.page.scss'],
})
export class DialPage implements OnInit {

val1='';
  constructor(private callNumber: CallNumber) { }

  ngOnInit() {
  }

  call(){
  this.callNumber.callNumber(this.val1, true)
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
  }
  

}
