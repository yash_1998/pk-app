import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DialPage } from './dial.page';

describe('DialPage', () => {
  let component: DialPage;
  let fixture: ComponentFixture<DialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
