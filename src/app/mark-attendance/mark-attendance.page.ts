import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Pedometer } from '@ionic-native/pedometer/ngx';
import { NgZone } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';

import { CrudService } from './../service/crud.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { firestore } from 'firebase/app';
import { async } from '@angular/core/testing';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

//for http
import { HttpModule, Http, Headers, Response } from '@angular/http';
import { HttpClient, HttpEventType } from '@angular/common/http';
import 'rxjs/add/operator/map';
import * as moment from 'moment';
import { catchError, map, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-mark-attendance',
  templateUrl: './mark-attendance.page.html',
  styleUrls: ['./mark-attendance.page.scss'],
})
export class MarkAttendancePage implements OnInit {
  // base64Url = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAbuklEQVR4nO2de2xdxZ3HP9y1LMuyvFEaRVEURVEUogihiE2zJrJc82jKIpryKtAWAgWW0oUWSqFs221ZtkIoQogNj7IFwqPA8i5lKVBeaXiFVwgmtbI0hJCGNGVDCCaY4BrH8ewfPw++vrnX99x7zzkzc87vI43sJPY9vzM5v+/5zW9+M7OfQckITcAMYBYwFWgbbQDtQKHoZ/uBkdHvh4CB0e93jX7dPfrvu0a/9o+23aM/r2SEJtcGKDXRBMwBDgAOBPZHHH7GaEvj/3MIEYM+RCD6gO3ANuAdoAfYgApFEOynEYCXFJC39wJgIfAPiNPPBVod2hWVAaAXEYPXGBOFYcYiD8UDVAD8wL7Zu4GDEac/gGxFaP2MicKLwCokelBBcIgKgBsKSMh+KPDl0a8zGD9OzzrDwFpgJfDk6PdDqCCkigpAejQDBwHHAUchY/g8OXw1+oDnETFYCWxBREJJEBWAZGkFOoFjgCVIwk6pzgiSM/gNcN/o9xoZJIAKQPw0IW/6U4DjgZluzQmeYWANcA/wW2TGQcUgJlQA4qGAvN1PAr6FhvdJMQA8hYjBE4zVKyh1ogLQGC1I5v67yLi+xa05uWIn8D/ACmRmQfMFdaACUDsFpNLuZOAMZLpO3/buGAaeBa5BkoeDTq0JDBWA6BSQufrzgKXAJLfmKGVYB/wKSR72ObYlCFQAqmOTej9AknohVOLlna3Ar4FbkBJlzRNUQAWgMk3IFN7FwBHIPL4SFruA+4HlwEZUCPZBBWBfCsi4/mfIG18dP3z6gZsQIdBpxCJUAMYoIHP2FwGnM7aUVskO2xERuBnNEQAqAJapwNlIgm+qY1uU5NkILEOShbsd2+KUvAtAOzKd92O0TDdvjCALkC5HiotyOX2YVwFoRmrzL0Ey/Ep+GUbqBy5FBCFX+YE8CsBM4DLgm2iCTxmjH7gaKSjKTX4gTwLQjGT1l6HhvlKZNciQcDU5KC/OgwDYhTrLEAHI0i47SjLsBm4ArgR2OLYlUbIuAK1IqP8LZMcdRamFXiQaWElGo4GsCkAB2UBzGZLs07e+Ui+DwK3IbMF7jm2JnSwKQDPy1r8CmObYFiU7bEDWg6wkQzMFWVvGOhlx/BWo8yvxMg94ADifDC0Iy1IEMA+4Dljs2hAl0wwDdyO5ge2ObWmYLAhAATgScf7Zjm1R8sMa4BxkD4JghwShDwFagR8hO8eq8ytp0gE8RuAFZSFHANORedpvEr6QKeEyiFQQXsHY4arBEKoALES2flro2hBFGeVRZDXpFsd21ERoAlBAjtG6Dd1vX/GPNchGsW+6NiQqIYXOBeBoZE94dX7FRzqQqcJgItNQBKAJ2Yn3NnTDDsVvDkBEoJsA/Mt7A5EM67nImF+34lZCYBYyM3UUnvuY18Yh03w/QTKsmam+UnLBNOB2ZJbK27UoPicB25FVfN/H4w5UlCr0I1WDN+PhikJfBWAScBWyO6/vUYqiVGMQ2Wb+l8CQY1vG4aMAtCEFPv/i2hBFiZFB5JCZG/AoEvDt7dqCKOVZrg1RlJhpQfan8Kpy1RtDkHH++cCF6JhfySZtyKK1o/HE93wZAhSQgzmWI0qpKFlmB/AtYJVrQ3wQgAJwEnAjkvlXlDywFTgRKR92hmsBKCAn796F7OajKHliAyIC610Z4FoAupCKqeluzVAUZ/QgIrDZxcVdCsA84BFgjjsTFMULViI5gZ1pX9hVJnIykg1V51cU2cfyUhzsLORCAJqRuX7dvFNRxjgbB5WvaQ8BCsiy3hUEvI+aoiREH3AMci5hKqQtAB3IuF/X9CtKedYDX0WmCRMnzXBjOrKmX51fUSpzILIQri2Ni6UlAK3Imv4FKV1PUULmeFIqiU9DAArIjj7fTOFaipIFCsgeAkcnfaE0cgCLgQfRMl9FqZWtwFeAjUldIOkIYAoS+qvzK0rtzESOJU9sgVySAtAEXISO+xWlEY4FTkjqw5McAnQBj5NSNlNRMsxW4BASOHUoqQjA7umnzq8ojTMTuIwEiueSEIACsrNPRwKfrSh55SRkOBArSQwBOoCn0cSfosTNJuAwYFtcHxh3BNCO7Oirzq8o8TMHuIQYC4TiFIACspV3d4yfqSjKeE4Djozrw+IcAswDXkS39lKUpOlFZgV2NfpBcUUATUjpojq/oiTPfCQSaJi4IoBFwDPolt6KkhZbgYOB7Y18SBwRQDOSmFDnV5T0mAmcR4M+HEcEsAR4CD3NR1HSpg+JAjbV+wGNRgBtyP5+6vyKkj6Tkdxb3f7XaARwOnALnpxz5hlDiDL3Its9z0NyJVoeXZ0+4BWk/5qQXXIWoftIlmMA+BJyvkDtmPrbZANvGTDa9mlvGVhqYGpRf7UYOMLAywb2emCjj22PgQcNLDTQXNR3bQbONvC+Bzb62B4q6a/IrV7nLxj4Vw9u3Le218CTBuZN0HdTDdxo4DMP7PWpfWzg34w4e6VnrtvAGx7Y6lv7zMjLJTUBmGbg/zy4cZ/aHgMrzPi3fqXWYuAnBj7xwG4f2l8NnGSgKULfzTHwtAc2+9aeM3VEAfUKwM89uGGf2t8MLDOV317lWpOBswx86IH9LtvbBg6vod8wMN3AA0aHUsVtr4HFNfZjbT882iYb+IsHN+xL+8TAj0x9Y7CCgeNNfqOp/zXQUUe/2edwhZHIy/V9+NIeMdGiqIYE4HwPbtSX9rGR5FRNnV6mHWnyJ6pvGJjfYL+1G7jOaD7Fts8MLKilD+vpcM38S/vQwGmmcee37VADf/bgvtJorxqYG1O/tRm4wqgI2HaLkcgyEQFY6sEN+tA+NHCyic/5bes28I4H95dU22skWTUr5n5rMfDvRnIxru/RdfvEwOyoffd3/0FkmpAjvWdG/5VM0ofUYN8PDMf82e8iBTCHAF+I+bNdM4Icenkqcp9xMgy8PHqNTvJdmdoM7AH+gPj4xNSgsouMJlw+Msm8+UvbQpO9odZzBmYm3G8tRmZj8j4ceN/ApCh9FrWEtwCcQ76VtR+4mGTe/KWsBU4hwRNhUuZ55H6SPvF2ENk992qkFDuvTCXqBqIRlXWGyXfRyqcGvm+Sf/OXiwTe9uD+6217DTxj5PlJs9/aDFxv8l0n8KKJ8LxGjQBOIr+LWIaAZcBNJP/mL2Ut8C0aWO7pkBFgJWJ/bLvYRmQ3skr17lE78kgHsohqYiKoaZOB1z1QNBdtj4HlRsaWab7BQo8E7JqIKGXRSbZpBh7zoD9ctWuq9VHUhy+vodQ9RmofXD7EoYmAL85v22wDr3nQLy7a+6bK8xtlCHBqxJ/LGi8hSb9+14aMshb4Bn4PB2zYfyqww7Etls3A90h/GOIDU5EduypTRT1bjazUcq1kLpRzUZW+cdUWGD8jAd/e/MWtYOBMk89CocfNBJWB1d7sXcD0Kj+TRe4A1rg2ogI9+BcJ+PjmL2YEuBcpRMobhzLBdv0TCUAByeDmjUHgYfzOHvskAr47v2UA+X/NGy1MMAyYSABagKNiN8d/dgLvuTYiAlYENju0IRTnt6zDb2FPiq9TwdcnEoBDkSRC3igQTtKzBzgRNyJgnf8UwnB+kErWPArA4cCkcv8w0YN+XDK2eM80ZEFJKLgQgRFgFeL8O1O8biMUgK+Rz3L2ViocKFpJAJoQ1cgjBeCHiBCEQg8S5qUlAquQ/FAozg+wEFjq2giHlB0GVBKAucCsJK3xnPnIcWch7UO/jnREwJb3huT8k4EryOeQ1rKYMsf3VRKAxRP8Wx4oAGcCZxFWPyQtAiE6fwtwOdDt2hDHtAMLSv+y3MNdAL6SuDn+04IsLT3atSE1YkVgS8yfG6LzNwE/QsQ8JCFPin2H9WWqg1qNbHbpuoLJl/ZXU/u21T60g0x8eww+bWCKB/dUa/Xf2UaWcrt+hnxpz5iSqsByHXe4B4b61t4x/pYGJy0CITo/Rg4a+Sim//+stL+ZksVB5cKifyrzd3lnNnAbkhwMiXXIdO6WOn8/xLAfZDec66kw951jWpBDVj+nVAAKSAJQ2Zd5wJ2jX0NiHXAMtYvASqTSMDTnPxJx/imuDfGULxf/oVQAphLeWy5N5iMiMMe1ITXSS20iYJ2/LymDEuII5Lj6PC5gi0oXRX5fKgALyGelVC0sBO4iuyIQqvN3AytQ56/GfIp8vFQADkrXlmDpQHICM1wbUiNWBCrtzruKcJ3/dvTMiii0AQfYP5QKwBfTtSVoupBIIEQR+Br7isAqZE1BaM7fhTj/LMd2hMRC+02xABTQCKBWusmGCKxCiodCdP47Ueevlc9f9MUCMBntyHroBu4hTBH4KrJ19teBXW7NqRkbgc1ybEeIdNhv9jNjf3k4cp6YUh+rcbMHfqM0E94pOtb5dcxfH0PA3wODxRHAPgsFlJroIsxIQJ0/fzQzWs9SLACaAGycUEUgFNT542MujBcATQDGg4pAMqjzx8scGBOANkYVQYkFKwJalBIP6vzxsz+MCcAsdL103HQB96Ei0CidiJiq88fLuAhAw9VkUBFojE6k//T5jB8VgJRQEagPdf5kmQa0WwHQpZPJoiJQG+r86TDbCsAXnJqRD7qAB1ARqIY6f3pM1QggXezDrSJQHnX+dJlSPA2opINGAuVR50+faVYA2p2akT86UREoZhHq/C5oswIQ0gk4WUFFQFgEPIg6vxM0AnCLFYGQziGME+v8eRdBZ1gB0H0A3dGJOEHeRECd3wM0AvCDTuAh8iMC6vyeYAVgn1NDldRZRD5EQJ3fI1QA/CLrIqDO7xm6AtA/sioCHch9qfN7hAqAn1gRmOrakJjoAB4me6IWPCoA/rIIcZrQRUCd32NUAPwmdBFQ5/ccFQD/GSC8nXstIdueC1QA/Mae2BPaoR2W9ZQ/hkzxBBUAfwnd+S2VziJUPEAFwE/sQZ2hO7/FisAWx3YoJagA+MdKwjyltxr2aPItju1QirACMODUCsWyEvgG2XN+i4qAZ1gB0Eyte7Lu/BYVAY+wApCVsWaorEROFs6681t6geNQEXCOFYBhp1bkG+v8O10bkjLrUBFwjhWA3U6tyC95dX6LioBjrAAMOrUin+Td+S0qAg7RCMAN6vzjsSKw2bUhOaPfCkC/UzPyhTp/edYhlY8qAumx3QpAXrLPLhlBnb8aKgLp0mcF4AOnZmQfdf7orEMqIVUEkuc9KwD6UCaHdf5T0H6OSg8qAkkzAmy2ArDNpSUZRp2/flQEkmUbMGAFQJdqxo91/lNR568XKwKbXBuSQTbD2DTgFnQ9QJwUO/8Ox7aETg+yRkJFIF42wfhCoA3ubMkU6vzxoyIQP2/B+P0A1jkyJEuo8ydHDzKLoiIQD+MiAIA3HBmSFdT5k2ctKgJxsY8A9DgyJAuE7PyhHQunItA4A8BGGC8AvciDrNSGdf5vE57zdwAPEN65AyoCjdHLaNK/WAD60Q6tlWLn3+7Yllqxh3YsIczDR1QE6ufzaL9YAEbQRGAtjABPEbbz2xN7Qj2ByIrARteGBMZr9pvSXYFfT9mQULFv/jMI3/ktiyr8ve+sRSotVQSiUzYCAI0AojACrAa+Q3jOX83JQz2a3IqADgeqs5uimp9yAqD7A07MaiTbH1r59CLgQao7d8gicAa6s1A1einy8VIB2IlOB07EGuQhC9X5p9fw8yGKwGokJxPa/0+avETRbF+pANixrbIvvcibP7TVabU6f+nvhSYCzyPDs/dcG+Ipfyj+Q7mjwZ5MyZCQeBNx/tASTfU6v6Wzwd93xVOICOgqzPEMIhHA55QTgDXoQSHFbELC/l7XhtRIo85v6USKhUITgd8D56HPcjFrKNkAuJwADAHPpmFNAGwHzkE6LiTicn5LqCJwP/AzdNt7y7OUVPuWE4AR4Ok0rPGcAeDHhJcTScpZQxSBEeBm4Fp0dgtKxv8AmPJtroG9BkxO214DVxhoqtA/vrZOA39JuG9eMDDdg3utpbUbeCThfvG9fWygtbRvykUAIJnuPBdVrAGuJKy3RidwHzAj4et0EV4k0I8MBfK89+UqygyFKgnA8Ogv5JFh4BrCyiCn5fyl1wtJBHqBO1wb4ZAHKLPat5IAgBSC5JHtSFVZKKTt/JYuwhOBJwkrqouLAeCJcv8wkQCsRospfMeV81tCFIGJnvmssooK06ETdcYA8Ggi5vjNFMJ4oF07vyUkEVhAPgXgQSps9lOtM+6L3xbvaUHOp/P5QbFO59r5Lb7ZU4524BjXRjhgECmKKku1h/wl8rmwYinyUPtIF3AX/jlbF3AP/tkF8pwvRaKmvPEsEyS0qwnAIFJNlTcmA8uB2a4NKcE6/0zXhlTAVxHoAi4Bml0b4oA7mWivzwhFFAcZ2ONBIYOL9pCByRH6KI3WZeBdD/okSnvBwAwP+gwD8wz80YM+cdHeN1IEVbF/ooxz15PfPQKOBpYBbY7t8P3NX4ovkcAM4HpgvmM7XHE/UgRVkSgCMIyEEXmkAJwJXIq7/fNDc36LFQFXdk9BCroOd3R914wAt1X9qYhh1DQDH3kQ0rhqnxn4uYHmiP0VZ9j/Zw/uv5H2soE5Kfdbu4HbTL7Xs7xsIqxliTrVtYN8JgMtzcBPgXNJL5HUhURes1K6XlIsQiKBOSldrx0Zti3F76ncpLmdKFWPNajqApPfZKBtnxo4P4qyNti6Tfhv/tL2qkk+Emg1cJ3R5/RDA1Oi9Fktndtk4A8e3Jzr9rGBs01yInCoyZ7z25akCLQaWcL9mQf36botN1CI0m+1dvLxHtycDy0pETjSJL+e33V71cjUXJz91mbU+W371Mh+HpH6rh6Vzeucamn7xMAFJr7E4FEG/urBfaXR/mhgfkz91m7geqNhv213mohvf1OHAGDgTA9u0pf2qYELTWMiUDASWb3vwf2k2f5koKOBfsPAJCPZfnV+aXtq7dN6FfdtD27Wl/Y3A5cZCUPrcf7TDHzgwX24aO8YSXjW8xxOM3CPyfdUX2l73NQ4LK1XeS/w4GZ9ap8ZCUNrKRtuNjKj8LEH9rts7xpYUkO/YWCWgceMOn9x22skh1STL9crAFNMOHXpaf4H3Gei1cC3GbjcSPTg2m4f2gdGIqEob68DjKw1cG2zb+1FAy0R+i8WAcDAuR7ctI/tBSMLqCr12wwDdxkdt5a2T4wMpSZV6LeCkTfcnzyw1be2x8ARFfotMQFoNzojUKm9a0QgZ5ixjGybgWNH+0xD1/Jtj4EnjdRCFL/NJhtJtn7ogY0+tkdMnYno/QwNcRJS5pnnkstKDCObqWwA+pBS2INwt6goJHYjK1A3I6XXc5G+a3JplKcMAl+izo1sGxWAFmSn1e7GPkZRlDq5Ffgude523KgAACwGHiOfu60oikt2AQfTwKnVcYTuz5PP3YMVxTU30OAJXnFEAAALgeeA1ng+TlGUKmxD3v4Nnd0RV/JuHXB3TJ+lKEp1rkJOsWqIuCIAkCz3y8hWTIqiJMd64BBkdqkh4py+2wxczkRbECuK0iiDyO5UDTs/xCsAI8DNyEEEiqIkw6+pcNBnPcQ5BLAsBJ4GJsX/0YqSazYChxHjob1JVPD1AFcm8LmKkmeGgB8T84ndSQjACPBfyLmCiqLEwx1McMhnvSQxBLB0AY/j/lQdRQmdzUjoH/tBvUku4nkJuDbBz1eUPGBD/0RO6U5SAEaQE3bzeq6gosTB/cDvkvrwJIcAlsOBh5ATWxRFic4m4CvAlqQukMY6/meRwzXrWq6oKDmlH/geCTo/pCMAI8BN6FoBRYnKMPLSXJn0hdIYAlimAQ8DHeldUlGC5A7gHGAg6QulKQAgVYKPIGKgKMq+rAWOIeaCn0qkvZdfD3ARsqBBUZTx7EDe/Kk4P6QvACPAb4CrU76uovjOIPJyTHXa3MVuvkPAFSRQ1qgoAfNLZM4/1eX0aecAipmDJAUPcGeConjB74FTiWmNfy243M9/E/BtEp7nVBTPeQkZ96fu/OA2ArB0A/ehMwNK/ugFTqSBbb0bxYcTfVYD38GRAiqKIzYhYb8z5wc/BGAEGQP9ADkSSlGyzjZk+Nvr2hAfBABEBO4FLkZrBJRssxP4ZzzZMMcXAQCpf74V+AUyVagoWaMfSfglXuMfFZ8EAMTxrwb+E109qGSLAeCHwG/xaOt83wQAZAhwGSIEGgkoWWA3UuV3Bx45P/gxDViJVuBC4GfIMeSKEiJ9SIL7XjyMan0WAJAjx89GSof14FElNHYgU9yP4tmb3+K7AAA0AScD16CHjSjhsBWZ6nseT50fwhAAkFzFscCvgKmObVGUamxAnH+Na0OqEYoAgIjAocBtwEy3pihKRXqQCr83XRsSBR9nASoxAqzCce20okzAs8jzGYTzQ1gCYFkDHIeeQqz4wwiy6e0pyCk+wRCiAIAo7InIGYRaK6C4ZDcyVf1dUtzKKy5CygGUowU4DVgGTHZsi5I/tgDnAU/g4Rx/FEIXAJAophOZITjQsS1KfliFHNyxwbUhjRDqEKCYEWRPga8iG456O+eqZIIh5NDbEwnc+SEbEUAx7cAFyGmqWjmoxE0fsmT9v8lI7ilrAgBSObgEuAqY7dgWJTusRcb7a8hQlJlFAbDMRPYWOBlZU6Ao9bAbCfmXI5t5ZIosCwCI4x+FLCaa69gWJTxeQUL+Vwg0y1+NrAuAZTpwCXA6urRYqU4/MoS8Ftjl2JZEyYsAgOQGjkCiAZ0uVCrxPPLWX0uGxvqVyJMAWKYilVtnoTMFyhh9SEHZTUgEkAvyKAAg0UA3cCnQRTbqIZT6GEI27LgM2aY782/9YvIqAJZW4Gjgp8B8x7Yo6WILyC5Dwv5MzOvXSt4FwDIJWIqM/XSvgezTi4T7v0N2680tKgDjmYbUd5+LLi7KIluBK5FKvkxn96OiArAvBWAWUk68FE0UZoE+ZOn49cB2x7Z4hQpAZQpIXuAi4HhUCEKkD9moYzmydDdXCb4oqABUpwDMQY50Og0dGoTANmAFctTce6jjV0QFIDoFJEdwOrL7iyYL/eNN4DrgfvS4+UioANTHJOAE5JTXhUhdgeKGIWQa70bkmPlcZ/VrRQWgMZqABcge8CegZxakyRbkuK3bgU1kdLFO0qgAxEMB2YxkCRIVdKFRQRIMAk8hZ0M8hb7tG0YFIH6akKThscj25QtQMWiEIeAl4GHkaO1taFIvNlQAkqUJqSk4FjgGWISKQRQGkXH9w0i1nmbyE0IFID0KyMzBEuDLyGIknVIc4z2kNv9JJJm3A3X6xFEBcEMB2a3oIGAxcBgSHeSp2GgX8pZ/BjnlaT3i8Or0KaIC4AcFxPk7kWnFf0TEYSbZWKo8jGyh3QO8jmyx1YuM79XhHaIC4C8FYAqSRFyAiMJcZKdjn7c160em5dYjzt4DrEMy9ursnqECEB5NwAxECOYC+yOzDjOQOoQpJCsQg8juuDuR1XUbgbeRN/xmNGEXFCoA2WQSIga2tSF1CoWir5UYQt7W9msfYw6/E11Gmyn+H05PL2neAgTHAAAAAElFTkSuQmCC';
  scannedCode = ""; start: boolean; splitted = []; splitted_0 = ""; splitted_1 = ""; splitted_2 = "";
  PedometerData: any;
  base64Url = '';
  stepCount: any = 0;
  location: any;
  date = moment()
  userEmail = ""; userFirstName = ""; userLastName = ""; userFullName = "";
  show_year_month_date = ""; device_entry_time: any; device_entry_time_as_number: any;
  phno = '';
  progress: number;
  userData: any = {};
  fileSize = '';
  uploaded = '';
  uploadTask: any;
  digitalOceanUpload: any;
  constructor(
    private _http: Http,
    public http: HttpClient,
    public geolocation: Geolocation,
    public toastController: ToastController,
    private ngZoneCtrl: NgZone,
    public platformCtrl: Platform,
    public pedoCtrl: Pedometer,
    private camera: Camera,
    private barcodeScanner: BarcodeScanner,
    private storage: Storage,
    public cdr: ChangeDetectorRef,
    private firestore: AngularFirestore,
    private crudService: CrudService, ) {
    this.getLocation();
  }

  ngOnInit() {
    // var file = this.dataURLtoFile(this.base64Url,new Date().getTime()+ '.png');
    // console.log(file);
    // this.fileSize = this.formatFileSize(file.size , 2);
    // console.log(this.formatFileSize(file.size , 2));
    // this.progress = 50;
    this.storage.get('userEmail').then((val) => {
      console.log('Your mail is', val);
      this.userEmail = val;
    });

    this.storage.get('userName').then((val) => {
      console.log('Your F Name is', val);
      this.userFirstName = val;
    });

    this.storage.get('userData').then((val) => {
      console.log('Your mail is', val);
      this.userData = val;
    });

    this.storage.get('userlName').then((val) => {
      console.log('Your L name is', val);
      this.userLastName = val;
    });


    // var str = "a,b,c";
    // this.splitted= str.split(","); 
    // console.log(this.splitted);
    // console.log(this.splitted[0]);
    // const currDate= new Date().toISOString();
    // alert(currDate);
    const currDate2 = new Date();
    // alert(currDate2);
    const estimatedServerTimeMs = new Date().getTime();
    console.log(new Date().getHours());
    console.log(new Date().getMinutes());
    console.log(new Date().getSeconds());
    //  alert(new Date(estimatedServerTimeMs));
    let a1 = (new Date(estimatedServerTimeMs));
    let mnth = a1.getMonth();
    console.log(mnth);
    let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    var mnth_name = months[mnth];
    console.log(mnth_name);
    let show_year_month_date = a1.getDate() + '-' + mnth_name + '-' + a1.getFullYear();
    console.log(show_year_month_date); // ok

    this.stepCount = 0;
  }


  b64toBlob() {
    var myBaseString = this.base64Url;

    // Split the base64 string in data and contentType
    var block = myBaseString.split(";");
    // Get the content type
    var contentType = block[0].split(":")[1];// In this case "image/png"
    // get the real base64 content of the file
    var b64Data = block[1].split(",")[1];// In this case "iVBORw0KGg...."

    // The path where the file will be created
    contentType = contentType || '';
    var sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }


  formatFileSize(bytes, decimalPoint) {
    if (bytes == 0) return '0 Bytes';
    var k = 1000,
      dm = decimalPoint || 2,
      sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
      i = Math.floor(Math.log(bytes) / Math.log(k));
    return { size: parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i], value: parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) } as any;
  }

  getLocation() {

    console.log("fetching location");
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords.latitude);
      console.log('Response => ', resp);

    }).catch((error) => {
      console.log('Error getting location', error);
    });

    // For Live location update
    const watch = this.geolocation.watchPosition();
    watch.subscribe((data: any) => {
      this.location = data.coords;
      console.log(this.location);
      // alert(this.location);
    });
  }


  getLocationalert() {

    console.log("fetching location");
    this.geolocation.getCurrentPosition().then((resp) => {
      console.log(resp.coords.latitude);
      console.log('Response => ', resp);

    }).catch((error) => {
      console.log('Error getting location', error);
    });

    // For Live location update
    const watch = this.geolocation.watchPosition();
    watch.subscribe((data: any) => {
      this.location = data.coords;
      //  console.log(this.location);
      // console.log(this.location?.longitude);
      console.log(this.location.latitude);
      console.log(this.location.longitude);
      console.log(this.location.accuracy);
      alert(this.location.latitude);
      alert(this.location.longitude);
      alert(this.location.accuracy);

    });
  }


  takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.base64Url = base64Image;
      this.date = moment();
    }, (err) => {
      // Handle error
    });
  }

  getDataFormat() {
    return this.date.format('DD/MM/YYYY hh:mm');
  }

  scanCode() {
    this.barcodeScanner.scan().then(
      barcodeData => {
        this.scannedCode = barcodeData.text;

        console.log('Barcode data', barcodeData);
        //alert(JSON.stringify(barcodeData));
        // alert(barcodeData.text)
        const currDate = new Date().toISOString();
        //alert(currDate);
        const currDate2 = new Date();
        //alert(currDate2);
        var str = barcodeData.text;
        //alert("str: "+str);
        this.splitted = str.split(",");
        this.splitted_0 = this.splitted[0];
        this.splitted_1 = this.splitted[1];
        this.splitted_2 = this.splitted[2];
        this.get_device_date_time();

        // alert(this.splitted);
        // alert(this.splitted_0);
        // alert(this.splitted_1);
        // alert(this.splitted_2);
      }
    )
  }


  async get_device_date_time() {
    alert("time");
    const currDate2 = new Date();
    // alert(currDate2);
    const estimatedServerTimeMs = new Date().getTime();
    console.log(new Date().getHours());
    console.log(new Date().getMinutes());
    console.log(new Date().getSeconds());
    this.device_entry_time = (new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
    this.device_entry_time_as_number = (new Date().getHours() + "" + new Date().getMinutes() + "" + new Date().getSeconds());
    //  alert(new Date(estimatedServerTimeMs));
    let a1 = (new Date(estimatedServerTimeMs));
    let mnth = a1.getMonth();
    console.log(mnth);
    let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    var mnth_name = months[mnth];
    console.log(mnth_name);
    let show_year_month_date = a1.getDate() + '-' + mnth_name + '-' + a1.getFullYear();
    console.log(show_year_month_date); // ok
    this.show_year_month_date = show_year_month_date;
    await this.saveEntry();
  }


  saveEntry() {
    alert("save");
    let record = {};
    record['location'] = this.splitted_0;
    record['company'] = this.splitted_1;
    record['companyId'] = this.splitted_2;
    record['entryDateserver'] = firebase.firestore.FieldValue.serverTimestamp();

    record['useremail'] = this.userEmail;
    record['username'] = this.userFirstName + " " + this.userLastName;

    record['devicedate'] = this.show_year_month_date;
    record['devicetime'] = this.device_entry_time;
    record['devicetimeasnumber'] = this.device_entry_time_as_number;

    record['pedometer'] = this.stepCount;
    record['longitude'] = this.location.longitude;
    record['latitude'] = this.location.latitude;
    record['accuracy'] = this.location.accuracy;

    this.crudService.create_NewscanLog(record).then(async resp => {
      this.splitted_0 = "";
      this.splitted_1 = "";
      this.splitted_2 = "";
      this.scannedCode = "";
      console.log(resp);
      await this.notificationToast();
    })
      .catch(error => {
        console.log(error);
        alert(error);
      });

  }

  cancelRequest() {
    this.progress = null;
    if (this.digitalOceanUpload) {
      this.digitalOceanUpload.unsubscribe();
    }
    if (this.uploadTask) {
      this.uploadTask.cancel();
    }
  }


  storeMarkAttendenace(type) {
    this.progress = 1;
    // this.crudService.showLoader();
    //Usage example:
    const file = this.b64toBlob();
    // var file = this.dataURLtoFile(this.base64Url, new Date().getTime() + '.png');
    const tempObject = this.formatFileSize(file.size, 2);
    this.fileSize = tempObject.size;
    if (tempObject.value < 10.00) {
      this.uploadTask = null;
      const formData = new FormData();
      formData.append("file", file);
      formData.append('company_name', this.userData.company_name.replace(/\s/g, ""));
      this.digitalOceanUpload = this.http
        .post("https://us-central1-officeplus-ab43b.cloudfunctions.net/uploadImage", formData, {
          reportProgress: true,
          observe: "events"
        })
        .pipe(
          tap((resp: any) => {
            if (resp && resp.body) {
              this.storeData(type, resp.body.url);
            }
          }),
          map((event: any) => {
            if (event.type == HttpEventType.UploadProgress) {

              this.progress = Math.round((100 / event.total) * event.loaded);
              if (event.loaded) {
                this.uploaded = this.formatFileSize(event.loaded, 2);
                this.cdr.detectChanges();
              }
            } else if (event.type == HttpEventType.Response) {
              this.progress = null;
            }
          }),
          catchError((err: any) => {
            this.progress = null;
            alert(err.message);
            return throwError(err.message);
          })
        ).subscribe()

    } else {
      this.digitalOceanUpload = null;
      this.fileSize = this.formatFileSize(file.size, 2).size;
      const path = `officeplus/pk/${this.userData.company_name}/`;
      const storageRef = firebase.storage().ref();
      this.uploadTask = storageRef.child(path).put(file);
      this.uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          // upload in progress
          this.uploaded = this.formatFileSize(snapshot.bytesTransferred, 2);
          this.cdr.detectChanges();
          const percentage = Math.trunc((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
          this.progress = percentage;
        },
        (error) => {
          this.progress = null;
          // upload failed
          console.log(error);

        },
        () => {
          // downloading url
          this.uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
            // return downloadedUrl
            this.progress = null;
            this.storeData(type, downloadURL);
            // if (this.scanData && this.scanData.id) {
            //   this.createLogUrls(downloadURL);
            // }
          }).catch(error => {
            this.progress = null;
            console.log('error : ' + error);

          });
        }
      );
    }

  }


  storeData(type, url) {
    const body = {} as any;
    body.long = this.location.longitude ? this.location.longitude : 0;
    body.lat = this.location.latitude ? this.location.latitude : 0;
    body.date = this.date.valueOf();
    body.userId = this.userData.authId;
    body.type = type;
    body.image = url;
    this.crudService.create_NewsMarkAttendance(body).then(res => {
      this.crudService.update_NewsMarkAttendance(res.id, { ...body, docId: res.id }).then(resp => {
        this.base64Url = '';
        if (type == 'in') {
          this.crudService.showToast('Mark in successfully');
        }
        if (type == 'out') {
          this.crudService.showToast('Mark out successfully');
        }
      })
    });
  }


  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }




  async notificationToast() {
    alert("notification");
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: "Scanned Sucessfully !",
      cssClass: "toast-scheme ",
      color: 'warning',
      buttons: [{
        //    side: 'start',
        //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'top',
      duration: 3000
    });
    toast.present();
    this.call_phpmsg();
  }

  fnGetPedoUpdate() {
    if (this.platformCtrl.is('cordova')) {
      this.pedoCtrl.startPedometerUpdates()
        .subscribe((PedometerData) => {
          this.PedometerData = PedometerData;
          this.ngZoneCtrl.run(() => {
            this.stepCount = this.PedometerData.numberOfSteps;
            // this.startDate = new Date(this.PedometerData.startDate);
            // this.endDate = new Date(this.PedometerData.endDate);
          });
        });
      this.start = true;

      // this.fnTost('Please Walk🚶‍to Get Pedometer Update.');
    } else {
      //	this.fnTost('This application needs to be run on📱device');
      console.log("ok");
    }
  }

  fnStopPedoUpdate() {
    this.stepCount = 0;
    this.pedoCtrl.stopPedometerUpdates();
    this.start = false;

  }

  call_phpmsg() {
    this.phpmsg().subscribe(resp => {
      console.log(resp);
    });
  }


  phpmsg() {
    this.phno = '9216142737';
    console.log('inside function');
    let phno = this.phno;
    let msg = "Patrolling Team Rana24x7Services visited your premises on " + this.device_entry_time;
    let params = '&phno=' + this.phno + '&msg=' + msg;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this._http.post("https://sapphireprofits.com/sendgrid/" + "send_sms_client.php", params, { headers: headers })
      .map((response: Response) => response.json());
  }

}
