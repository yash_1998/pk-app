import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogsyesterdayPageRoutingModule } from './logsyesterday-routing.module';

import { LogsyesterdayPage } from './logsyesterday.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogsyesterdayPageRoutingModule
  ],
  declarations: [LogsyesterdayPage]
})
export class LogsyesterdayPageModule {}
