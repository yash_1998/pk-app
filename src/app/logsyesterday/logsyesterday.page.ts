import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';


import { CallLog, CallLogObject } from '@ionic-native/call-log/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';

import { CrudService } from './../service/crud.service';
import * as firebase from 'firebase';
import { firestore } from 'firebase/app';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-logsyesterday',
  templateUrl: './logsyesterday.page.html',
  styleUrls: ['./logsyesterday.page.scss'],
})
export class LogsyesterdayPage implements OnInit {

  filters: CallLogObject[];filters_o: CallLogObject[];
  recordsFound: any; recordsFound_o: any;
  recordsFoundText: string; recordsFoundText_o: string;
  listTyle:string;
  today_or_yesterday='';

  epoch_val = 1590645517326;

  from_time: any;
  from_time_o_y: any;
  from_time_o_t: any;

  constructor(
    private crudService: CrudService,
    private callLog: CallLog, 
    private callNumber: CallNumber,
    private platform: Platform
  ) {
 
  //   this.epoch_val = 1590645517326;
  //   let a1 = (new Date(this.epoch_val)); // converted to date time lambi string
  //  let mnth = a1.getMonth();
  //             console.log(mnth);
  //             let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
  //             var mnth_name = months[mnth];
  //             console.log(mnth_name);
  //             let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
  //             console.log(show_year_month_date);


    this.platform.ready().then(() => {

      this.callLog.hasReadPermission().then(hasPermission => {
        if (!hasPermission) {
          this.callLog.requestReadPermission().then(results => {
          //  this.getContacts("type","1","==");
          })
            .catch(e => alert(" requestReadPermission " + JSON.stringify(e)));
        } else {
      //    this.getContacts("type", "1", "==");
        }
      })
        .catch(e => alert(" hasReadPermission " + JSON.stringify(e)));
    });
   }

  ngOnInit() {
  }

  getContacts(name, value, operator) {
    if(value == '1'){
      this.listTyle = "Incoming";
    }else if(value == '2'){
      this.listTyle = "Outgoing";
    }else if(value == '3'){
      this.listTyle = "Missed";
    }

    //Getting Yesterday Time
    var today = new Date();
    today.setHours(9, 30, 0);
    var yesterday = new Date(today);
    yesterday.setDate(today.getDate() - 1);
    var fromTime = yesterday.getTime();
    this.from_time = yesterday.getTime();

  
    alert(fromTime.toString());
    this.filters = [{
      name: name,
      value: value,
      operator: operator,
    }, {
      name: "date",
      value: fromTime.toString(),
      operator: ">=",
    },
    // {
    //   name: "duration",
    //   value: value,
    //   operator: operator,
    // }
  
  
  ];
    this.callLog.getCallLog(this.filters)
      .then(results => {
        this.recordsFoundText = JSON.stringify(results);
    //    console.log(this.recordsFoundText);
        this.recordsFound = results;//JSON.stringify(results);
   //     console.log(this.recordsFound);
      })
      .catch(e => alert(" LOG " + JSON.stringify(e)));
  }




  callThisNumber(number){
    this.callNumber.callNumber(number, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  


  sendLogs(){

    let record = {};
    record['name'] = "saurabh";

    if(this.listTyle == "Incoming"){
      record['type'] = "Incoming";
    }

    if(this.listTyle == "Outgoing"){
      record['type'] = "outgoing";
    }

    if(this.listTyle == "Missed"){
      record['type'] = "Missed";
    }


//changing epoch value to readable format, 28-MAY-2020
    this.epoch_val = this.from_time;
    let a1 = (new Date(this.epoch_val)); // converted to date time lambi string
   let mnth = a1.getMonth();
              console.log(mnth);
              let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
              var mnth_name = months[mnth];
              console.log(mnth_name);
              let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
              console.log(show_year_month_date);

    record['date'] = show_year_month_date;
    record['outgoing'] = this.recordsFound;
   // record['recordstext'] = this.recordsFoundText;

    this.crudService.create_NewLog(record).then(resp => {
      console.log(resp,resp.id);
      if(resp.id != null){
      console.log("saved successfully");
      alert("saved successfully");
      }
    
    })
      .catch(error => {
        console.log(error);
      });

  }

}

