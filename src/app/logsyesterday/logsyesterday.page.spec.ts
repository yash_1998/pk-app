import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LogsyesterdayPage } from './logsyesterday.page';

describe('LogsyesterdayPage', () => {
  let component: LogsyesterdayPage;
  let fixture: ComponentFixture<LogsyesterdayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsyesterdayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LogsyesterdayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
