import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogsyesterdayPage } from './logsyesterday.page';

const routes: Routes = [
  {
    path: '',
    component: LogsyesterdayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogsyesterdayPageRoutingModule {}
