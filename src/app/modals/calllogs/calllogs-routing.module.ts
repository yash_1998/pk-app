import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalllogsPage } from './calllogs.page';

const routes: Routes = [
  {
    path: '',
    component: CalllogsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalllogsPageRoutingModule {}
