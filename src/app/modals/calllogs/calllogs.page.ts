import { Component, OnInit,Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { CrudService } from './../../service/crud.service';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-calllogs',
  templateUrl: './calllogs.page.html',
  styleUrls: ['./calllogs.page.scss'],
})
export class CalllogsPage implements OnInit {
  @Input() public lunch: string;
  @Input() public clicked_number: string;
  public dinner = "rajma";
  val1='';val2='';val3='';val4='';val5='';val6='';entryDate_show='';
  userEmail: string;
  userId: string;
  userNm: string ='';userlNm: string =''; userFullname=''; userAuthid='';
  constructor(
    private storage: Storage,
    public modalController: ModalController,
    private firestore: AngularFirestore,
    private crudService: CrudService,
    ) { 
      if(this.clicked_number == null){
        this.clicked_number = '';
        console.log("no clicked number found")
      }
    }

  ngOnInit() {

          
              //fetch date time from server and convert to 27-APR-2020 for entryDate_show
              var offsetRef = firebase.database().ref(".info/serverTimeOffset");
              offsetRef.on("value", (snap) => {
                var offset = snap.val();
                var estimatedServerTimeMs = new Date().getTime() + offset;
                console.log(estimatedServerTimeMs); // enoch numeric string
                console.log(new Date(estimatedServerTimeMs)); // converted to date time lambi string
           // console.log(new Date(1584877396545));
                let a1 = (new Date(estimatedServerTimeMs));
                let mnth = a1.getMonth();
                console.log(mnth);
                let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
                var mnth_name = months[mnth];
                console.log(mnth_name);
                let show_year_month_date =  a1.getDate() +'-'+ mnth_name +'-'+ a1.getFullYear();
                console.log(show_year_month_date);
                this.entryDate_show = show_year_month_date;
                console.log(this.entryDate_show);
              });



    this.storage.get('userEmail').then((val) => {
      console.log('Your mail is', val); 
      this.userEmail = val;
    });
    this.storage.get('userName').then((val) => {
      console.log('Your F Name is', val);
      this.userNm = val;
    });
  
    this.storage.get('userlName').then((val) => {
      console.log('Your L name is', val);
      this.userlNm = val;
      this.userFullname = this.userNm +' '+ this.userlNm;
      console.log(this.userFullname);
    });

    this.storage.get('loggedInUser').then((val) => {
      console.log('User auth id is', val);
      this.userAuthid = val;
    });
  }

  async closeModal() {
    await this.modalController.dismiss(this.dinner);
    };


    CreateNewCalllog(){


              timestamp: firebase.firestore.FieldValue.serverTimestamp();

      let record = {};
      record['number'] = this.clicked_number;
      record['partyname'] = this.val1;
      record['fromtime'] = this.val2;
      record['totime'] = this.val3;
      record['typeofcall'] = this.val4;
      record['remarks'] = this.val5;
      record['regarding'] = this.val6;
      record['entryDate'] = firebase.firestore.FieldValue.serverTimestamp();
      record['entryDate_show'] = this.entryDate_show;
      record['fullname'] = this.userFullname;
      record['useremail'] = this.userEmail;
      record['authid'] = this.userAuthid;
  
      
   
      this.crudService.create_NewCalllog(record).then(resp => {
        this.clicked_number= "";
        this.val1="";
        this.val2="";
        this.val3="";
        this.val4="";
        this.val5="";
        this.val6="";
        console.log(resp);
        if(resp.id != null){
          alert("Saved Successfully");
        }
      })
        .catch(error => {
          console.log(error);
        });
  
    }  
  


}
