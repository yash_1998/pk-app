import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalllogsPageRoutingModule } from './calllogs-routing.module';

import { CalllogsPage } from './calllogs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalllogsPageRoutingModule
  ],
  declarations: [CalllogsPage]
})
export class CalllogsPageModule {}
