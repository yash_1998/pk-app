import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CalllogsPage } from './calllogs.page';

describe('CalllogsPage', () => {
  let component: CalllogsPage;
  let fixture: ComponentFixture<CalllogsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalllogsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CalllogsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
