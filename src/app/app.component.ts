import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import * as firebase from 'firebase/app';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Dashboard',
      url: '/dashboard',
      icon: 'paper-plane'
    },
    //    {
    //   title: 'Call',
    //   url: '/home',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Logs Today',
    //   url: '/logs',
    //   icon: 'mail'
    // },
    // {
    //   title: 'View Call Register',
    //   url: '/viewcalllogs',
    //   icon: 'mail'
    // },

{
  title: 'Mark Attendance',
      url: '/mark-attendance',
      icon: 'mail'
},
    // patrolling guard round
    {
      title: 'Start Round',
      url: '/gscan',
      icon: 'mail'
    },

    // {
    //   // kranti meter service agent
    //   title: 'Service Agent',
    //   url: '/serviceagententry',
    //   icon: 'mail'
    // },

   // {
      // kranti meter service agent
    //   title: 'Service Agent',
    //   url: '/serviceagent-pshiba',
    //   icon: 'mail'
    // },

    // {
    //    //needed for yogesh [rajisthan] supervisor visit with sms
    //   title: 'Supervisor Log Visit',
    //   url: '/scan',
    //   icon: 'mail'
   
    // },


    // {
    //   title: 'Logs Yesterday',
    //   url: '/logsyesterday',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Dial',
    //   url: '/dial',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Contacts',
    //   url: '/contact',
    //   icon: 'mail'
    // },




    //no page below
    // {
    //   title: 'Inbox',
    //   url: '/folder/Inbox',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Outbox',
    //   url: '/folder/Outbox',
    //   icon: 'paper-plane'
    // },
    // {
    //   title: 'Favorites',
    //   url: '/folder/Favorites',
    //   icon: 'heart'
    // },
    // {
    //   title: 'Archived',
    //   url: '/folder/Archived',
    //   icon: 'archive'
    // },
    // {
    //   title: 'Trash',
    //   url: '/folder/Trash',
    //   icon: 'trash'
    // },
    // {
    //   title: 'Spam',
    //   url: '/folder/Spam',
    //   icon: 'warning'
    // }
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public geolocation: Geolocation,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    firebase.initializeApp(environment.firebaseConfig);
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.geolocation.getCurrentPosition().then(resp => {

      });
    });
  }

  ngOnInit() {
    // const path = window.location.pathname.split('folder/')[1];
    // if (path !== undefined) {
    //   this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    //}
  }
}
