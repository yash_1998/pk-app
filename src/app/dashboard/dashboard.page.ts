import { Component, OnInit } from '@angular/core';


import { NavController, ModalController,MenuController} from '@ionic/angular';
import { AuthenticateService } from '../service/authentication.service';

import { IonicStorageModule } from '@ionic/storage';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { UserService } from '../user.service';
import { AngularFirestore } from '@angular/fire/firestore';


//for popover
import {PopoverController} from '@ionic/angular';
//import {NotificationsPage} from '../popups/notifications/notifications.page';

//notification toast
import { ToastController } from '@ionic/angular';
import { reduce } from 'rxjs/operators';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'], 
  
})
export class DashboardPage implements OnInit {

  loggedInuser='';notifyarr: any;notification_length=0; i:any;
  userEmail: string;
  userId: string;
  userNm: string ='';userlNm: string =''; userAllowedPages=[];
  followers = ['brown','black'];

  constructor(
    public toastController: ToastController,
    private popover:PopoverController,
    private navCtrl: NavController,
    private authService: AuthenticateService,
    private storage: Storage,
    private firestore: AngularFirestore,
    private menuCtrl: MenuController,
    public user:UserService
  ) {
  } 

  ngAfterViewInit() {
   // this.cdRef.detectChanges();
 }

//  createpopup(i){
//   console.log(i);
//       this.popover.create({
//         component:NotificationsPage,
//         componentProps: { notification : this.notifyarr},
//         cssClass: 'custom-popover',
//         showBackdrop:false}).then((popoverElement)=>{
// //pass something here
//       popoverElement.present();
//       })
//     } 

  async ngOnInit(){
    this.storage.set('allowedPages',[1,2,3,4]);
    this.storage.get('allowedPages').then((val) => {
      console.log('Your pages is', val);
    });
 
    
    this.menuCtrl.enable(true);
     // Or to get a key/value pair
  this.storage.get('userEmail').then((val) => {
    console.log('Your mail is', val); 
    this.userEmail = val;
  });
  this.storage.get('userName').then((val) => {
    console.log('Your F Name is', val);
  });

  this.storage.get('userlName').then((val) => {
    console.log('Your L name is', val);
  });

  // this.storage.get('loggedInUser').then((val) => {
  //   console.log('Your user ID is', val);
  //   this.loggedInuser = val;
  // });

  this.storage.get('loggedInUser').then((val) => {
    if (val != null){
      console.log("some one is logged in");
      this.loggedInuser = val;
      console.log(this.loggedInuser);
      this.doc_check();
     // this.fetch_notification();

   //   this.navCtrl.navigateForward('/dashboard');
     // this.navCtrl.navigateRoot('/dashboard');
    }else{
      console.log("no one is logged in");
  //    this.storage.set('loggedInUser', this.userId);
  //    this.storage.set('userEmail', this.userEmail);
     // this.navCtrl.navigateBack('');
    }
    console.log('Your user ID is', val);
  });
    
    if(this.authService.userDetails()){
      this.userEmail = this.authService.userDetails().email;
      this.userId = this.authService.userDetails().uid;
      console.log(this.userId);
      this.storage.set('loggedInUser', this.userId);
      this.storage.set('userEmail', this.userEmail);
      this.loggedInuser = this.userId;
      console.log(this.loggedInuser);
      this.doc_check();

   //   this.storage.set('userName', this.user.registered_username);

   console.log("trying...");
   firebase.firestore().collection(`Students`)
   .where("authId", "==", this.userId) 
   .get()
   .then(querySnapshot => {
     querySnapshot.forEach((doc) => {
         console.log(doc.data().Name);
         this.userNm=doc.data().Name;
         this.userlNm=doc.data().lname;
         this.userEmail=doc.data().personalEmail;
         this.userAllowedPages = doc.data().allowedpages;
         this.storage.set('userName',this.userNm);
         this.storage.set('userlName',this.userlNm);
         this.storage.set('userEmail',this.userEmail);
         console.log(this.userNm,this.userlNm)
         console.log(this.userAllowedPages);
         this.storage.set('allowedPages',this.userAllowedPages);
         console.log(this.userAllowedPages);
         console.log(doc.id, " ===> ", doc.data());
     });
   });
   setTimeout(()=>{
   console.log(this.userNm);
  }, 1000);
  // this.storage.set('usernm',this.userNm);

    // var worksRef = firebase.firestore().collection("todos")
    // .where("authId", "==", this.userId)
    // .get().subscribe(querySnapshot => {
    //  console.log(query.data());

// firebase.firestore().collection("Students").where("authId", "==", this.userId)
//     .onSnapshot(function(querySnapshot) {
//         querySnapshot.forEach(function(doc) {
//           console.log("--name--");
//             console.log(doc.data().name);
//         });
        
//     });


    }
    
    else{
      //var allowedPages = this.storage.get('allowedPages');
      //console.log(allowedPages);
      this.storage.get('loggedInUser').then((val) => {
      if (val == null||""){
     console.log("First time naughty, go back to login");
     this.navCtrl.navigateBack('');
      }
    });
     
    }
      
//check notification doc of logged in user , created or not
 



  }

  logout(){
    console.log("clicked");
    this.authService.logoutUser()
    .then(res => {
      console.log(res);
      this.storage.set('loggedInUser', null);
      this.storage.set('userEmail', null);
      this.storage.set('userName',null);
      this.storage.set('userlName',null);
      this.storage.set('allowedPages',null);

      this.navCtrl.navigateBack('');
    })
    .catch(error => {
      console.log(error);
    })
  }

//1. check if the doc exists
  doc_check(){

  var docRef = firebase.firestore().collection('notifications').doc(this.loggedInuser);
  docRef.get().then(doc=> {
  if (doc.exists) {
    console.log("Document data:", doc.data());
  } else {
    // doc.data() will be undefined in this case
    console.log("No such document!");
  this.create_doc();
  }
  }).catch(function(error) {
  console.log("Error getting document:", error);
  });
  }

  //2.create doc
 create_doc(){
  firebase.firestore().collection("notifications").doc(this.loggedInuser).set({
  //  name: "Los Angeles visit",
    notifyArray: [],
   // age: 12,
  })
  .then(function() {
     console.log("Document successfully written!");
  })
  .catch(function(error) {
     console.error("Error writing document: ", error);
  });
  
  }



  bell(){
    
    console.log("call bell");
    this.bellagain();
  }
  bellagain()
  {
    if(this.user.stopbell == 'Y')
    {
      var audio = new Audio();
      audio.src= "assets/audio/clearly.mp3";
      audio.load();
      audio.play();
              
      // sample testing for order..
      // setTimeout(()=>{
      //   if(this.user.stopbell == 'Y')
      //   {
      //     audio.play();
      //     this.bellagain();
      //   }
        
      // },5000);
    }
  }

  stopbell()
  {
    if(this.user.stopbell == 'Y')
    {
      this.user.stopbell = 'N';
    }
    else
    {
      this.user.stopbell = 'Y';
    }
    
  }

  async notificationToast() {
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: "New Notification: "+this.notifyarr[this.notification_length]["desc"],
      cssClass: "toast-scheme ",
      color: 'warning',
      buttons:[        {
    //    side: 'start',
    //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'top',
      duration: 2000
    });
    toast.present();
  }
  

}
