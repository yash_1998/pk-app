//import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-serviceagent-pshiba',
//   templateUrl: './serviceagent-pshiba.page.html',
//   styleUrls: ['./serviceagent-pshiba.page.scss'],
// })
// export class ServiceagentPshibaPage implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }

import * as firebase from 'firebase';
import { firestore } from 'firebase/app';
import { Component, Inject, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Storage } from '@ionic/storage';
//import { NotificationsService } from '../notifications.service';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
import { Subject, Observable, Subscription, interval } from 'rxjs'; 
import { Platform, ToastController } from '@ionic/angular'
import { UploadService } from '.././service/upload.service';
import { AngularFireStorage,AngularFireStorageReference } from '@angular/fire/storage';
import { finalize } from "rxjs/operators";


@Component({
  // selector: 'app-serviceagententry',
  // templateUrl: './serviceagententry.page.html',
  // styleUrls: ['./serviceagententry.page.scss'],
     selector: 'app-serviceagent-pshiba',
   templateUrl: './serviceagent-pshiba.page.html',
   styleUrls: ['./serviceagent-pshiba.page.scss'],
})
export class ServiceagentPshibaPage implements OnInit {

  agent_desc='';
  selectedImage: any = null; file_name_vid='';vid_url:string = '';
  deferredPrompt; encodedImage=''; docid=''; fileURL=''; filename='';
  image: any;
  showBtn: true;
  public showWebcam = true;
  public allowCameraSwitch = true;
  public multipleWebcamsAvailable = false;
  shwClkImg=false;shwCamera=false;
  deviceId: any;
  public goalList =[];complaint_search_value=''; todays_date: any;
  constructor(
    @Inject(AngularFireStorage) private storage2: AngularFireStorage, 
    @Inject(UploadService) private fileService: UploadService,
    public toastController: ToastController,
    public camera: Camera,
    public storage: Storage,
   // public notification: NotificationsService,
  ) { 
    
  }



  ionViewWillEnter() {
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later on the button event.
      this.deferredPrompt = e;

      // Update UI by showing a button to notify the user they can add to home screen
      this.showBtn = true;
    });

    //button click event to show the promt

    window.addEventListener('appinstalled', (event) => {
      alert('installed');
    });


    if (window.matchMedia('(display-mode: standalone)').matches) {
      alert('display-mode is standalone');
    }
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.image = 'data:image/jpeg;base64,' + imageData;
      this.storage.set('Image', this.image);
    }, (err) => {
      // Handle error
    });
  }

    //Webcame 

    public videoOptions: MediaTrackConstraints = {
      // width: {ideal: 1024},
      // height: {ideal: 576}
    };
    public errors: WebcamInitError[] = [];
  
    // latest snapshot
    public webcamImage: WebcamImage = null;
  
    // webcam snapshot trigger
    private trigger: Subject<void> = new Subject<void>();
    // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
    private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();

    public ngOnInit(): void {



      WebcamUtil.getAvailableVideoInputs()
        .then((mediaDevices: MediaDeviceInfo[]) => {
          this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
        });
    }
  
    public triggerSnapshot(): void {
      this.trigger.next();
    }

    public toggleWebcam(): void {
      this.showWebcam = !this.showWebcam;
    }
  
    public handleInitError(error: WebcamInitError): void {
      this.errors.push(error);
    }

    public showNextWebcam(directionOrDeviceId: boolean | string): void {
      // true => move forward through devices
      // false => move backwards through devices
      // string => move to device with given deviceId
      this.nextWebcam.next(directionOrDeviceId);
    }
  
    public handleImage(webcamImage: WebcamImage): void {
      console.info('received webcam image', webcamImage);
      this.shwClkImg = true;
      this.shwCamera=false;
      this.webcamImage = webcamImage;
      console.log(webcamImage['_imageAsDataUrl']);
   //   this.encodedImage = webcamImage['_imageAsDataUrl'];
   this.encodedImage = webcamImage['imageAsBase64'];
      console.log(this.encodedImage);
    }

    public cameraWasSwitched(deviceId: string): void {
      console.log('active device: ' + deviceId);
      this.deviceId = deviceId;
    }
  
    public get triggerObservable(): Observable<void> {
      return this.trigger.asObservable();
    }
  
    public get nextWebcamObservable(): Observable<boolean | string> {
      return this.nextWebcam.asObservable();
    }
    callBtn()
    {
      this.shwCamera = true;
    }

  searchArray(){
    if(this.complaint_search_value !== ''){
      var query2 = firebase.firestore().collection(`complaints`).where("searchArray", "array-contains", this.complaint_search_value) 
    }
    else{
      var query2 = firebase.firestore().collection(`complaints`).where("searchArray", "array-contains", 0)
    }
    
    this.goalList =[];
//     let a=3;

  //   if(a==1){
  //  query2 =   firebase.firestore().collection(`complaints`)
  //     .where("searchArray", "array-contains", "22") 
  //     console.log("1");
  //   }

//     if(a==2){
//  query2 =   firebase.firestore().collection(`complaints`)
//     .where("searchArray", "array-contains", "23")
//     console.log("2");
//     }

//     if(a==3){
//       query2 =   firebase.firestore().collection(`complaints`)
//       .where("searchArray", "array-contains", this.complaint_search_value)
//       console.log("3");
//          }
    query2
    .onSnapshot(querySnapshot=> {   
      this.goalList =[];
      querySnapshot.forEach(doc=> {
          // console.log(doc.data());
          // console.log("Search result");
          // var data = Object.assign(doc.data(), {docid : doc.id})
          // this.goalList.push(data);
          var complaint_date: any =  new Date(doc.data().complaint_date_show);
          var current_date: any = new Date(this.todays_date);
          console.log(complaint_date);
          console.log(current_date);
          var duration: any  = (current_date - complaint_date)/86400000;
          console.log(duration);
          var data = Object.assign(doc.data(), {docid : doc.id}, {duration: duration})
          this.goalList.push(data);
      });
      this.docid = this.goalList[0].docid;
      console.log(this.goalList[0].docid);
      console.log(this.goalList[0].name);
      console.log(this.goalList[0].phno);
      console.log(this.goalList);
      });
  }

  upload_base64(){
    const randomId = Math.random().toString(36).substring(2);
    var storageRef = firebase.storage().ref().child('image3.jpeg');
   // var imageRef = " ";
   const uploadTastk = firebase.storage().ref().child('/master/'+randomId).putString(this.encodedImage, 'base64', {contentType:'image/jpeg'})
   uploadTastk.then(
       async ( response ) => {
        response.ref.getDownloadURL().then(async downloadURL=> {
          console.log("File available at", downloadURL);
          this.fileURL = downloadURL;
          this.filename = randomId;
          if(response.state == "success"){
            await this.updaterecord();
           // await this.notificationToast();
           }
        });
           console.log('image upload success')
           



       }, 
       ( failedReason ) => {
           console.log('image upload failed')
       }
   )

   storageRef.getDownloadURL().then(function(url) {
     console.log(url);
 }); 

  }

  async notificationToast() {
    const toast = await this.toastController.create({
      header: 'Alert',
      message: "uploaded successfully !",
      cssClass: "toast-scheme ",
      color: 'warning',
      buttons:[        {
    //    side: 'start',
    //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'top',
      duration: 3000
    });
    toast.present();
  }

  async updaterecord(){
    console.log(this.filename);
    console.log(this.fileURL);
    
    var washingtonRef = firebase.firestore().collection("complaints").doc(this.docid);
    
    // Set the "capital" field of the city 'DC'
    return washingtonRef.update({
       // file_link : this.tags
       filename: this.filename,
       fileURL: this.fileURL,
       agent_desc: this.agent_desc,
    })
    .then(async res=> {
      console.log(res);
      this.fileURL='';
      this.filename='';
      //this.docid='';
     await this.notificationToast();
        console.log("Document successfully updated!");
        
    })
    .catch(function(error) {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
    });
    
      }

      cancel(){

      }
 
      fresh_data(){
        
      }

      showPreview(event: any) {
        this.selectedImage = event.target.files[0];
        }

      video_upload(){
        var name = this.selectedImage.name;
        this.file_name_vid = this.selectedImage.name;
        const fileRef = this.storage2.ref('/vid/'+name);
        this.storage2.upload('/vid/'+name, this.selectedImage).snapshotChanges().pipe(
        finalize(() => {
        fileRef.getDownloadURL().subscribe(async (url) => {
        this.vid_url = url;
        
        console.log(this.vid_url);
       // console.log(this.id); //coming undefined
        //this.fileService.insertImageDetails(this.id,this.url);
        alert('Upload Successful');
        await this.update_video_link_in_db();
        })
        })
        ).subscribe();
      }

      update_video_link_in_db(){
        console.log(this.file_name_vid);
        console.log(this.vid_url);
        
        var washingtonRef = firebase.firestore().collection("complaints").doc(this.docid);
        
        // Set the "capital" field of the city 'DC'
        return washingtonRef.update({
           // file_link : this.tags
           vidname: this.file_name_vid,
           vidURL: this.vid_url,
           agent_desc: this.agent_desc,
        })
        .then(async res=> {
          console.log(res);
          this.vid_url='';
          this.file_name_vid='';
          //this.docid='';
         await this.notificationToast();
            console.log("Document successfully updated!");
            
        })
        .catch(function(error) {
            // The document probably doesn't exist.
            console.error("Error updating document: ", error);
        });
      }
}

