import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ServiceagentPshibaPage } from './serviceagent-pshiba.page';

describe('ServiceagentPshibaPage', () => {
  let component: ServiceagentPshibaPage;
  let fixture: ComponentFixture<ServiceagentPshibaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceagentPshibaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ServiceagentPshibaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
