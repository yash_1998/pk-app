import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServiceagentPshibaPageRoutingModule } from './serviceagent-pshiba-routing.module';

import { ServiceagentPshibaPage } from './serviceagent-pshiba.page';

//cam
import { WebcamModule } from 'ngx-webcam';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WebcamModule,
    ServiceagentPshibaPageRoutingModule
  ],
  declarations: [ServiceagentPshibaPage]
})
export class ServiceagentPshibaPageModule {}
