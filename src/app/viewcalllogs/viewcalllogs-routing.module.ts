import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewcalllogsPage } from './viewcalllogs.page';

const routes: Routes = [
  {
    path: '',
    component: ViewcalllogsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewcalllogsPageRoutingModule {}
