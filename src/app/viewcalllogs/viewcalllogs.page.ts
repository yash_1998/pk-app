import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Storage } from '@ionic/storage';
import { CrudService } from './../service/crud.service';

@Component({
  selector: 'app-viewcalllogs',
  templateUrl: './viewcalllogs.page.html',
  styleUrls: ['./viewcalllogs.page.scss'],
})
export class ViewcalllogsPage implements OnInit {

  public goalList:any[]; cd='cd'; loggedInusr=''; dt1=''; selectedDate='';
  public goalList2:any[];
  constructor(private storage: Storage) { }

  ngOnInit() {
    this.storage.get('loggedInUser').then((val) => {
      console.log('Your user ID is', val);
      this.loggedInusr = val;
    });
  } 

  fetchDate($event){
    console.log($event);
    //change format of complaint_date , date ek aage se 0 prefix hataya hai, like 06 nahi, 6, because call register
    //mein aise hi save karvaya hai
let dt = new Date(this.dt1);let dt1='';let dd = dt.getDate();let mon = dt.getMonth();let yy = dt.getFullYear();
let months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
if(dd < 10){dt1 = ""+dd;}else{dt1 = dd.toString();}var date1 = dt1+'-'+months[mon]+'-'+yy;

//console.log(date1);
this.selectedDate = date1;
console.log(this.selectedDate);
  }

  fetchRecords(){
    this.goalList =[];
    
    console.log(this.loggedInusr);
    firebase.firestore().collection("calllogs")
.where("entryDate_show", "==", this.selectedDate)
.where("authid", "==", this.loggedInusr)
//.limit(2)
.get()
.then(querySnapshot=> {
  querySnapshot.forEach(doc=> {
     var data = Object.assign(doc.data(), {docid : doc.id})
      this.goalList.push(data);
      console.log(this.cd);
  });
  console.log(this.goalList);
});
}



}
