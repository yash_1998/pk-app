import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewcalllogsPage } from './viewcalllogs.page';

describe('ViewcalllogsPage', () => {
  let component: ViewcalllogsPage;
  let fixture: ComponentFixture<ViewcalllogsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewcalllogsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewcalllogsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
