import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewcalllogsPageRoutingModule } from './viewcalllogs-routing.module';

import { ViewcalllogsPage } from './viewcalllogs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewcalllogsPageRoutingModule
  ],
  declarations: [ViewcalllogsPage]
})
export class ViewcalllogsPageModule {}
