import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ServiceagententryPage } from './serviceagententry.page';

const routes: Routes = [
  {
    path: '',
    component: ServiceagententryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceagententryPageRoutingModule {}
