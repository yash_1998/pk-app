import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServiceagententryPageRoutingModule } from './serviceagententry-routing.module';

import { ServiceagententryPage } from './serviceagententry.page';

//cam
import { WebcamModule } from 'ngx-webcam';
 
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WebcamModule,
    ServiceagententryPageRoutingModule
  ],
  declarations: [ServiceagententryPage]
})
export class ServiceagententryPageModule {}
