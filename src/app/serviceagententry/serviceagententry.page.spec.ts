import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ServiceagententryPage } from './serviceagententry.page';

describe('ServiceagententryPage', () => {
  let component: ServiceagententryPage;
  let fixture: ComponentFixture<ServiceagententryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceagententryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ServiceagententryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
