import { Component, OnInit } from '@angular/core';
import { CrudService } from './../service/crud.service';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  search_array = []; val1=''; val2=''; val3='';
  constructor(private firestore: AngularFirestore) { }

  ngOnInit() {
  }


  saveContact(){
 var washingtonRef = firebase.firestore().collection("contacts").doc("contacts");
washingtonRef.update({
    regions: firebase.firestore.FieldValue.arrayUnion({name: this.val1 , phno1: this.val2 , phno2: this.val3 })
});
  }

  deleteContact(){
    var washingtonRef = firebase.firestore().collection("contacts").doc("contacts");
    washingtonRef.update({
    regions: firebase.firestore.FieldValue.arrayRemove(
      { name: "mehak", phno1: "1", phno2:"2" },
    )
   });
  }

}
