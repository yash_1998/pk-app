import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'logs',
    loadChildren: () => import('./logs/logs.module').then( m => m.LogsPageModule)
  },
  // {
  //   path: 'dial',
  //   loadChildren: () => import('./dial/dial.module').then( m => m.DialPageModule)
  // },
  // {
  //   path: 'contact',
  //   loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  // },
  {
    path: 'calllogs',
    loadChildren: () => import('./modals/calllogs/calllogs.module').then( m => m.CalllogsPageModule)
  },
  {
    path: 'viewcalllogs',
    loadChildren: () => import('./viewcalllogs/viewcalllogs.module').then( m => m.ViewcalllogsPageModule)
  },
  {
    path: 'gscan',
    loadChildren: () => import('./gscan/gscan.module').then( m => m.GscanPageModule)
  },
  {
    path: 'scan',
    loadChildren: () => import('./scan/scan.module').then( m => m.ScanPageModule)
  },
  {
    path: 'serviceagententry',
    loadChildren: () => import('./serviceagententry/serviceagententry.module').then( m => m.ServiceagententryPageModule)
  },
  {
    path: 'serviceagent-pshiba',
    loadChildren: () => import('./serviceagent-pshiba/serviceagent-pshiba.module').then( m => m.ServiceagentPshibaPageModule)
  },
  {
    path: 'serviceagent-pshiba2',
    loadChildren: () => import('./serviceagent-pshiba2/serviceagent-pshiba2.module').then( m => m.ServiceagentPshiba2PageModule)
  },
  {
    path: 'mark-attendance',
    loadChildren: () => import('./mark-attendance/mark-attendance.module').then( m => m.MarkAttendancePageModule)
  },
  {
    path: 'vide-preview',
    loadChildren: () => import('./vide-preview/vide-preview.module').then( m => m.VidePreviewPageModule)
  },
  // {
  //   path: 'logsyesterday',
  //   loadChildren: () => import('./logsyesterday/logsyesterday.module').then( m => m.LogsyesterdayPageModule)
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
